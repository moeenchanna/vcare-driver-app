package com.easy.vcare.driver.data.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginPasswordRequest {

    @SerializedName("contact_no")
    @Expose
    private String contactNo;

    public LoginPasswordRequest(String pass, String contactNo) {
        this.contactNo = contactNo;
        this.password = password;
    }

    @SerializedName("password")
    @Expose
    private String password;



    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
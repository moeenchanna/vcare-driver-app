package com.easy.vcare.driver.data.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendOtpRequest {
    @SerializedName("contact_no")
    @Expose
    private String contactNo;

    public SendOtpRequest(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

}
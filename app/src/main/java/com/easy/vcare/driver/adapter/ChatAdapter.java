package com.easy.vcare.driver.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.data.model.MessageFormat;
import com.easy.vcare.driver.ui.activity.chat.ChatRoomActivity;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ChatAdapter extends ArrayAdapter<MessageFormat> {
    public ChatAdapter(Context context, int resource, List<MessageFormat> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, @NotNull ViewGroup parent) {
        Log.i(ChatRoomActivity.TAG, "getView:");

        MessageFormat message = getItem(position);

        assert message != null;
        if(TextUtils.isEmpty(message.getMessage())){


            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.user_connected, parent, false);

            TextView messageText = convertView.findViewById(R.id.message_body);

            Log.i(ChatRoomActivity.TAG, "getView: is empty ");
            String userConnected = message.getUsername();
            messageText.setText(userConnected);

        }else if(message.getUniqueId().equals(ChatRoomActivity.uniqueId)){
            Log.i(ChatRoomActivity.TAG, "getView: " + message.getUniqueId() + " " + ChatRoomActivity.uniqueId);


            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.my_message, parent, false);
            TextView messageText = convertView.findViewById(R.id.message_body);
            messageText.setText(message.getMessage());

        }else {
            Log.i(ChatRoomActivity.TAG, "getView: is not empty");

            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.their_message, parent, false);

            TextView messageText = convertView.findViewById(R.id.message_body);
            TextView usernameText = (TextView) convertView.findViewById(R.id.name);

            messageText.setVisibility(View.VISIBLE);
            usernameText.setVisibility(View.VISIBLE);

            messageText.setText(message.getMessage());
            usernameText.setText(message.getUsername());
        }

        return convertView;
    }
}

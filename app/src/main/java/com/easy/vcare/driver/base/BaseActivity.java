package com.easy.vcare.driver.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;


import com.easy.vcare.driver.R;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

public class BaseActivity extends AppCompatActivity {

    public Socket mSocket;
    /**
     * Permissions Properties
     **/
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5445;
    protected static final String TAG = BaseActivity.class.getSimpleName();
    /**
     * Static Members
     **/
    public static int fragment_value;
    public static int ride_service_value;
    public static int assistance_service_value;
    /**
     * Duration of wait
     **/
    public final int SPLASH_DISPLAY_LENGTH = 1000;

    public String platform = "android";
    public String deviceid = "12344";
    public static String AUTHORIZE_TOKEN;
    public static String DRIVER_ID ="1";

    public Bundle extras;
    /**
     * Map Properties
     **/
    public GoogleMap googleMap;
    public FusedLocationProviderClient fusedLocationProviderClient;
    public Marker currentLocationMarker;
    public Location currentLocation;

    /**
     * Intent Properties
     **/
    public boolean firstTimeFlag = true;
    public Intent mainIntent;

    /**
     * Toolbar Properties
     **/
    public TextView mTitle;
    protected ProgressDialog mProgressDialog;

    /**
     * Static Members Properties
     **/
    public static int getFragment_value() {
        return fragment_value;
    }

    public static int getRide_service_value() {
        return ride_service_value;
    }

    public static int getAssistance_service_value() {
        return assistance_service_value;
    }

    public static void hideKeyboardwithoutPopulate(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                Objects.requireNonNull(activity.getCurrentFocus()).getWindowToken(), 0);
    }

    public void baseToolbar(String title) {
        mTitle.setText(title);
    }

    /**
     * Themes Properties
     **/
    public void statusbarTheme() {
        //Change status bar color
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void mapstatusbarTheme() {
        //Change status bar color
        if (Build.VERSION.SDK_INT >= 21) {
            Window w = getWindow();
            w.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            w.setStatusBarColor(0x00000000);
        }

    }

    public void intentTransition() {
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    /**
     * Map Styling Theme
     **/
    public void mapStyle() {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.uber_style));

            if (!success) {
                Log.e("MapsActivityRaw", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivityRaw", "Can't find style.", e);
        }
    }

    /*Toast Method*/
    public void toastCall(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void snackbarCall(String msg) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG);
       /* snackbar.setAction("Dismiss", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
        snackbar.show();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);


    }

    protected void showProgressDialog(String title, String message) {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        mProgressDialog = ProgressDialog.show(this, title, message);

    }

    protected void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        // hide progress dialog to prevent leaks
        hideProgressDialog();
    }
}

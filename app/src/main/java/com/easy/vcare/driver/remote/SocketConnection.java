package com.easy.vcare.driver.remote;

import android.app.Application;
import android.util.Log;

import com.easy.vcare.driver.untils.Constants;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class SocketConnection extends Application {
    private static final String TAG = "SocketConnection";

    public static Socket mSocket;
    static {

        try {
            final IO.Options options = new IO.Options();
            options.transports = Constants.TRANSPORTS;
            mSocket = IO.socket(Constants.SOCKET_URI, options);
            Log.e(TAG, "Socket Connection Create: ");

        } catch (URISyntaxException e) {
            Log.e(TAG, "instance initializer: "+e );
            throw new RuntimeException(e);
        }

    }

    public Socket getSocket() {
        return mSocket;
    }
}
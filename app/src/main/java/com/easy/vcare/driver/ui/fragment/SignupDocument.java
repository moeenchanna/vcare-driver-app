package com.easy.vcare.driver.ui.fragment;


import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.ui.activity.MainActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;


public class SignupDocument<adapter> extends  Fragment implements View.OnClickListener {
    private static final String TAG = "SignupDocument";
    ArrayList<String> array_image = new ArrayList<String>();

    private Button mFrontBtnCnic;
    private Button mBackBtnCnic;
    private Button mFrontBtnLisence;
    private Button mBackBtnLisence;
    private Button mBtnProfile;
    private Button mOtherBtnDocu;
    private ImageView mFrontImageCnic;
    private ImageView mBackImageNic;
    private ImageView mFrontImageLisence;
    private ImageView mBackImageLisence;
    private ImageView mImageProfile;
    private ImageView mOtherImageDocu;
    private ImageView imageContainer ;
    private Button mSubmit;

    TextView textView;

    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_GALLARY = 2;

    Bitmap bitmap;
    Dialog dialog;
    Button gallery, camera, cancel;
    boolean dailogstatus = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_signup_document, container, false);
        initView(view);
        imageContainer = null;


        return view;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PICK_FROM_CAMERA:
                if (resultCode == getActivity().RESULT_OK) {
                    //pic coming from camera
                    bitmap = null;
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    Log.d(TAG, "onActivityResult: " + imageBitmap);
                    imageContainer.setImageBitmap(imageBitmap);

                }
                break;

            case PICK_FROM_GALLARY:

                if (resultCode == getActivity().RESULT_OK) {
                    //pick image from gallery
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    // Get the cursor
                    assert selectedImage != null;
                    Cursor cursor = requireActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // Move to first row
                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imgDecodableString = cursor.getString(columnIndex);

                        array_image.add(imgDecodableString);
                        Arrays.asList(array_image).indexOf(6); //pass index

                    // Log.e(TAG, "imgDecodableString: " + imgDecodableString);
                    cursor.close();
                    InputStream is = null;
                    try {
                        is = getActivity().getContentResolver().openInputStream(Uri.parse(imgDecodableString));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    //Bitmap bitmap = BitmapFactory.decodeStream(is);
                    Uri fullPhotoUri = data.getData();
                    imageContainer.setImageURI(fullPhotoUri);
                    //bitmap = BitmapFactory.decodeFile(imgDecodableString);
                    Log.e(TAG, "bitmap: " + fullPhotoUri);
                    Log.e(TAG, "selectedImage: " + selectedImage);

                }
                break;

        }
    }

    private void initView(@NonNull final View itemView) {


        ///Images
        mFrontImageCnic = itemView.findViewById(R.id.cnic_front_image);
        mBackImageNic = itemView.findViewById(R.id.nic_back_image);
        mFrontImageLisence = itemView.findViewById(R.id.lisence_front_image);
        mBackImageLisence = itemView.findViewById(R.id.lisence_back_image);
        mImageProfile = itemView.findViewById(R.id.profile_image);
        mOtherImageDocu = itemView.findViewById(R.id.docu_other_image);


        mFrontBtnCnic = (Button) itemView.findViewById(R.id.cnic_front_btn);
        mFrontBtnCnic.setOnClickListener(this);
        mBackBtnCnic = (Button) itemView.findViewById(R.id.cnic_back_btn);
        mBackBtnCnic.setOnClickListener(this);
        mFrontBtnLisence = (Button) itemView.findViewById(R.id.lisence_front_btn);
        mFrontBtnLisence.setOnClickListener(this);
        mBackBtnLisence = (Button) itemView.findViewById(R.id.lisence_back_btn);
        mBackBtnLisence.setOnClickListener(this);
        mBtnProfile = (Button) itemView.findViewById(R.id.profile_btn);
        mBtnProfile.setOnClickListener(this);
        mOtherBtnDocu = (Button) itemView.findViewById(R.id.docu_other_btn);
        mOtherBtnDocu.setOnClickListener(this);
        mSubmit = (Button) itemView.findViewById(R.id.submit);
    }

    private void showDialog() {
        dialog = new Dialog(this.getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.selectimage);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();
        dailogstatus = true;

        gallery = dialog.findViewById(R.id.btn_gallery);
        camera = dialog.findViewById(R.id.camera);
        cancel = dialog.findViewById(R.id.btn_cancel);

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePhotoFromGallary();
                dialog.cancel();

            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhotoFromCamera();
                dialog.cancel();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

    }


    private void takePhotoFromCamera() {
        Log.d(TAG, "takePhotoFromCamera: ");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, PICK_FROM_CAMERA);
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, PICK_FROM_GALLARY);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cnic_front_btn:
                imageContainer = mFrontImageCnic;
                showDialog();
                // TODO 20/10/13
                break;
            case R.id.cnic_back_btn:
                imageContainer = mBackImageNic;
                showDialog();
                // TODO 20/10/13
                break;
            case R.id.lisence_front_btn:
                imageContainer = mFrontImageLisence;
                showDialog();

                // TODO 20/10/13
                break;
            case R.id.lisence_back_btn:
                imageContainer = mBackImageLisence;
                showDialog();

                // TODO 20/10/13
                break;
            case R.id.profile_btn:
                imageContainer = mImageProfile;
                showDialog();

                // TODO 20/10/13
                break;
            case R.id.docu_other_btn:
                imageContainer = mOtherImageDocu;
                showDialog();
                // TODO 20/10/13
                break;
            default:
                break;
        }
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: "+array_image);
               /* Intent intent = new Intent(SignupDocument.this, MainActivity.class);
                startActivity(intent);*/


            }
        });
    }


}

package com.easy.vcare.driver.data.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RideViewResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("errors")
    @Expose
    private List<Object> errors = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public List<Object> getErrors() {
        return errors;
    }

    public void setErrors(List<Object> errors) {
        this.errors = errors;
    }


    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("booking_id")
        @Expose
        private String bookingId;
        @SerializedName("customer_id")
        @Expose
        private Integer customerId;
        @SerializedName("driver_id")
        @Expose
        private Integer driverId;
        @SerializedName("vehicle_type_id")
        @Expose
        private Integer vehicleTypeId;
        @SerializedName("coupon_id")
        @Expose
        private Object couponId;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("canceled")
        @Expose
        private Integer canceled;
        @SerializedName("payment_mode")
        @Expose
        private String paymentMode;
        @SerializedName("paid")
        @Expose
        private Integer paid;
        @SerializedName("distance")
        @Expose
        private String distance;
        @SerializedName("travel_time")
        @Expose
        private Object travelTime;
        @SerializedName("start_address")
        @Expose
        private String startAddress;
        @SerializedName("start_latitude")
        @Expose
        private String startLatitude;
        @SerializedName("start_longitude")
        @Expose
        private String startLongitude;
        @SerializedName("end_address")
        @Expose
        private String endAddress;
        @SerializedName("end_latitude")
        @Expose
        private String endLatitude;
        @SerializedName("end_longitude")
        @Expose
        private String endLongitude;
        @SerializedName("track_distance")
        @Expose
        private Object trackDistance;
        @SerializedName("track_latitude")
        @Expose
        private Object trackLatitude;
        @SerializedName("track_longitude")
        @Expose
        private Object trackLongitude;
        @SerializedName("track_accuracy")
        @Expose
        private Object trackAccuracy;
        @SerializedName("start_at")
        @Expose
        private Object startAt;
        @SerializedName("finished_at")
        @Expose
        private Object finishedAt;
        @SerializedName("route_key")
        @Expose
        private Object routeKey;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("driver_arrival_pic")
        @Expose
        private Object driverArrivalPic;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBookingId() {
            return bookingId;
        }

        public void setBookingId(String bookingId) {
            this.bookingId = bookingId;
        }

        public Integer getCustomerId() {
            return customerId;
        }

        public void setCustomerId(Integer customerId) {
            this.customerId = customerId;
        }

        public Integer getDriverId() {
            return driverId;
        }

        public void setDriverId(Integer driverId) {
            this.driverId = driverId;
        }

        public Integer getVehicleTypeId() {
            return vehicleTypeId;
        }

        public void setVehicleTypeId(Integer vehicleTypeId) {
            this.vehicleTypeId = vehicleTypeId;
        }

        public Object getCouponId() {
            return couponId;
        }

        public void setCouponId(Object couponId) {
            this.couponId = couponId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Integer getCanceled() {
            return canceled;
        }

        public void setCanceled(Integer canceled) {
            this.canceled = canceled;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public Integer getPaid() {
            return paid;
        }

        public void setPaid(Integer paid) {
            this.paid = paid;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public Object getTravelTime() {
            return travelTime;
        }

        public void setTravelTime(Object travelTime) {
            this.travelTime = travelTime;
        }

        public String getStartAddress() {
            return startAddress;
        }

        public void setStartAddress(String startAddress) {
            this.startAddress = startAddress;
        }

        public String getStartLatitude() {
            return startLatitude;
        }

        public void setStartLatitude(String startLatitude) {
            this.startLatitude = startLatitude;
        }

        public String getStartLongitude() {
            return startLongitude;
        }

        public void setStartLongitude(String startLongitude) {
            this.startLongitude = startLongitude;
        }

        public String getEndAddress() {
            return endAddress;
        }

        public void setEndAddress(String endAddress) {
            this.endAddress = endAddress;
        }

        public String getEndLatitude() {
            return endLatitude;
        }

        public void setEndLatitude(String endLatitude) {
            this.endLatitude = endLatitude;
        }

        public String getEndLongitude() {
            return endLongitude;
        }

        public void setEndLongitude(String endLongitude) {
            this.endLongitude = endLongitude;
        }

        public Object getTrackDistance() {
            return trackDistance;
        }

        public void setTrackDistance(Object trackDistance) {
            this.trackDistance = trackDistance;
        }

        public Object getTrackLatitude() {
            return trackLatitude;
        }

        public void setTrackLatitude(Object trackLatitude) {
            this.trackLatitude = trackLatitude;
        }

        public Object getTrackLongitude() {
            return trackLongitude;
        }

        public void setTrackLongitude(Object trackLongitude) {
            this.trackLongitude = trackLongitude;
        }

        public Object getTrackAccuracy() {
            return trackAccuracy;
        }

        public void setTrackAccuracy(Object trackAccuracy) {
            this.trackAccuracy = trackAccuracy;
        }

        public Object getStartAt() {
            return startAt;
        }

        public void setStartAt(Object startAt) {
            this.startAt = startAt;
        }

        public Object getFinishedAt() {
            return finishedAt;
        }

        public void setFinishedAt(Object finishedAt) {
            this.finishedAt = finishedAt;
        }

        public Object getRouteKey() {
            return routeKey;
        }

        public void setRouteKey(Object routeKey) {
            this.routeKey = routeKey;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDriverArrivalPic() {
            return driverArrivalPic;
        }

        public void setDriverArrivalPic(Object driverArrivalPic) {
            this.driverArrivalPic = driverArrivalPic;
        }

    }

}
package com.easy.vcare.driver.untils;

import com.easy.vcare.driver.BuildConfig;
import com.github.nkzawa.engineio.client.transports.WebSocket;
import com.github.nkzawa.socketio.client.Socket;

public class Constants {

    public static String BASE_URL = BuildConfig.BASE_URL;


    //public static final String SOCKET_URI = "http://socket.nb-live.com";
    public static final String SOCKET_URI = "http://192.168.18.49:3000";

    public static final String[] TRANSPORTS = {WebSocket.NAME};
    public static Socket mSocket;

    public static String HEADERS_CONTENT_TYPE = "application/json";
    public static String HEADERS_Accept = "application/json";

    public static String COUNTRY = "pk";
    public static String LANGUAGE = "en";

    public static String MAP_KEY =  BuildConfig.MAP_KEY;

}

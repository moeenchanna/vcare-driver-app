package com.easy.vcare.driver.ui.activity.profile;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.data.responses.ProfileViewResponse;
import com.easy.vcare.driver.remote.ApiInterface;
import com.easy.vcare.driver.remote.ErrorUtils;
import com.easy.vcare.driver.remote.RetrofitClient;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileViewActivity extends BaseActivity {
    private static final String TAG = "ProfileViewActivity";

    private TextView mTextView;
    private TextView mTextname;
    private TextView mTextemail;
    private TextView mTextcontact;
    private TextView mTextaddress;
    private TextView mTextcnic;
    private TextView mTextlicense;
    private TextView mTextvichel;
    private LinearLayout mUpLayoutSign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_view);
        initView();
        mainIntent = getIntent();
        extras = mainIntent.getExtras();
        assert extras != null;
        AUTHORIZE_TOKEN = extras.getString("token");

        Log.e(TAG, "AUTHORIZE_TOKEN: " + AUTHORIZE_TOKEN);
    }

    private void initView() {
        mTextView = (TextView) findViewById(R.id.textView);
        mTextname = (TextView) findViewById(R.id.textname);
        mTextemail = (TextView) findViewById(R.id.textemail);
        mTextcontact = (TextView) findViewById(R.id.textcontact);
        mTextaddress = (TextView) findViewById(R.id.textaddress);
        mTextcnic = (TextView) findViewById(R.id.textcnic);
        mTextlicense = (TextView) findViewById(R.id.textlicense);
        mTextvichel = (TextView) findViewById(R.id.textvichel);
        mUpLayoutSign = (LinearLayout) findViewById(R.id.sign_up_layout);


        getProfileData();
    }

    @SuppressLint("LongLogTag")
    private void getProfileData() {

        Log.e(TAG, "registerRequest: " );

        ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);
        Call<ProfileViewResponse> call = apiInterface.getprofileView();
        Log.e(TAG, "registerRequest: " + call.toString());
        call.enqueue(new Callback<ProfileViewResponse>() {
            @Override
            public void onResponse(@NotNull Call<ProfileViewResponse> call, @NotNull Response<ProfileViewResponse> response) {                Log.e(TAG, "onResponse: "+response );

                try {
                    if (response.isSuccessful()) {
                        hideProgressDialog();
                        Log.e(TAG, "onResponse: " + response);

                        assert response.body() != null;

                        Log.e(TAG, "onResponse: " + response.body().getMessage());
                        Log.e(TAG, "onResponse: " + response.body().getStatus());

                        if (response.body().getStatus() == 200) {
                            hideProgressDialog();
                            Log.e(TAG, "onResponse: " + response);

                            ProfileViewResponse profileResponse = response.body();
                            String name = profileResponse.getData().getName();
                            String email = profileResponse.getData().getEmail();
                            String contactNo = profileResponse.getData().getContactNo();
                            String address = profileResponse.getData().getAddress();
                            String cnic = profileResponse.getData().getCnicNo();
                            String license = profileResponse.getData().getLicenseNo();
                            Integer vichel =  profileResponse.getData().getVehicleId();



                            mTextname.setText(name);
                            mTextemail.setText(email);
                            mTextcontact.setText(contactNo);
                            mTextaddress.setText(address);
                            mTextcnic.setText(cnic);
                            mTextlicense.setText(license);
                            mTextvichel.setText(vichel);



                            if (profileResponse != null) {

                                String response_msg = profileResponse.getMessage();
                                Log.e(TAG, "onResponse: " + response_msg);

                              /*  List<ProfileResponse.Data> profileResponses;
                                  profileResponses = response.body().getData();*/


                                toastCall(response_msg);
                                snackbarCall(response_msg);


                            }

                        } else {
                            if (response.errorBody() != null) {

                                Gson gson = new Gson();
                                ErrorUtils message = gson.fromJson(response.errorBody().charStream(), ErrorUtils.class);

                                Log.e(TAG, "Error Response: " + message.getStatus());
                                Log.e(TAG, "Error Response: " + message.getMessage());


                                Toast.makeText(ProfileViewActivity.this, message.getMessage(), Toast.LENGTH_SHORT).show();


                            }
                        }
                    }
                } catch (Exception e) {
                    hideProgressDialog();
                    Log.e(TAG, "catch: ", e);

                }

            }
            @Override
            public void onFailure(Call<ProfileViewResponse> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "onFailure: ", t);
                Log.d("error", Objects.requireNonNull(t.getMessage()));
                toastCall("Server Error");
            }
        });
    }

}
package com.easy.vcare.driver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.data.responses.RideViewResponse;

import java.util.List;

public class RideHistoryAdapter extends RecyclerView.Adapter<RideHistoryAdapter.RideHistoryView> {
    List<RideViewResponse.Datum> rideHistoryResponse;
    Context mContext;

    public RideHistoryAdapter(List<RideViewResponse.Datum> rideHistoryResponse, Context mContext) {
        this.rideHistoryResponse = rideHistoryResponse;
        this.mContext = mContext;
    }



    public void setRideList(List<RideViewResponse.Datum> rideHistoryResponse) {
        this.rideHistoryResponse = rideHistoryResponse;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RideHistoryView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View view= inflater.inflate(R.layout.recycler_ride_history_items, parent, false);
        return new RideHistoryView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RideHistoryView holder, int position) {


        holder.tv_pick.setText(rideHistoryResponse.get(position).getStartAddress());
        holder.tv_drop.setText(rideHistoryResponse.get(position).getEndAddress());
        holder.tv_pickdate.setText(rideHistoryResponse.get(position).getCreatedAt());
        holder.tv_picktime.setText(rideHistoryResponse.get(position).getUpdatedAt());




    }

    @Override
    public int getItemCount() {
        if(rideHistoryResponse != null){
            return rideHistoryResponse.size();
        }
        return 0;
    }

    public class RideHistoryView extends RecyclerView.ViewHolder {

        TextView tv_pick,tv_drop,tv_pickdate,tv_picktime;

        public RideHistoryView(@NonNull View itemView) {
            super(itemView);



            tv_pick = (TextView)itemView.findViewById(R.id.tv_picklocation_name);
            tv_drop = (TextView)itemView.findViewById(R.id.tv_droplocation_name);
            tv_pickdate = (TextView)itemView.findViewById(R.id.tv_picklocation_date);
            tv_picktime = (TextView)itemView.findViewById(R.id.tv_droplocation_time);
        }
    }
}

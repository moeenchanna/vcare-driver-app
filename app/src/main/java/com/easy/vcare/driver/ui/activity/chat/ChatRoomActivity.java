package com.easy.vcare.driver.ui.activity.chat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.adapter.ChatAdapter;

import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.data.model.ChatMessage;
import com.easy.vcare.driver.data.model.MessageFormat;
import com.easy.vcare.driver.remote.SocketConnection;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChatRoomActivity extends BaseActivity  {


    public static final String TAG = "ChatRoomActivity";
    public static String uniqueId;

    private EditText textField;
    private ImageButton sendButton;

    private String Username;

    private Boolean hasConnection = false;

    private ListView messageListView;
    private ChatAdapter chatAdapter;

    private Thread thread2;
    private boolean startTyping = false;
    private int time = 2;

    private String user_request_id;
    private String room;
    String user_type = "0";
    String attachment = "";

    @SuppressLint("HandlerLeak")
    Handler handler2=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.i(TAG, "handleMessage: typing stopped " + startTyping);
            if(time == 0){
                setTitle("SocketIO");
                Log.i(TAG, "handleMessage: typing stopped time is " + time);
                startTyping = false;
                time = 2;
            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);

        SocketConnection app = (SocketConnection) getApplication();
        mSocket = app.getSocket();
        //mSocket.connect();



        user_request_id = getIntent().getExtras().getString("user_request_id");
        room = getIntent().getExtras().getString("room");
        Log.e(TAG, "user_request_id: " + user_request_id);
        Log.e(TAG, "room: " + room);

        if(savedInstanceState != null){
            hasConnection = savedInstanceState.getBoolean("hasConnection");
        }
        if(hasConnection){

        }else {
            mSocket.connect();

            mSocket.on("receive_message", receive_message);
            mSocket.on("receive_all_messages", receive_all_messages);
            //mSocket.on("on typing", onTyping);

            JSONObject userId = new JSONObject();
            try {
                //userId.put("username", Username + " Connected");
                userId.put("user_request_id", user_request_id);
                userId.put("room", room);
                Log.e(TAG, "data: " + userId);
                // perform the sending message attempt.
                mSocket.emit("all_messages", userId);
               // mSocket.emit("connect user", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.i(TAG, "onCreate: " + hasConnection);
        hasConnection = true;


        Log.i(TAG, "onCreate: " + Username + " " + "Connected");

        textField = findViewById(R.id.textField);
        sendButton = findViewById(R.id.sendButton);
        messageListView = findViewById(R.id.messageListView);

        List<MessageFormat> messageFormatList = new ArrayList<>();
        chatAdapter = new ChatAdapter(this, R.layout.item_message, messageFormatList);
        messageListView.setAdapter(chatAdapter);

        onTypeButtonEnable();


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("hasConnection", hasConnection);
    }

    public void sendMessage(View view){
        Log.i(TAG, "sendMessage: ");
        String message = textField.getText().toString().trim();
        if(TextUtils.isEmpty(message)){
            Log.i(TAG, "sendMessage:2 ");
            return;
        }
        textField.setText("");
        JSONObject jsonObject = new JSONObject();
        try {
           /* jsonObject.put("message", message);
            jsonObject.put("username", Username);
            jsonObject.put("uniqueId", uniqueId);*/
            jsonObject.put("user_request_id", user_request_id);
            jsonObject.put("room", room);
            jsonObject.put("message", message);
            jsonObject.put("type", user_type);
            // perform the sending message attempt.
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Log.i(TAG, "sendMessage: 1"+ mSocket.emit("chat message", jsonObject));
        Log.i(TAG, "sendMessage: 1"+  mSocket.emit("send_message", jsonObject));
    }

    public void onTypeButtonEnable(){
        textField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

              /*  JSONObject onTyping = new JSONObject();
                try {
                    onTyping.put("typing", true);
                    onTyping.put("username", Username);
                    onTyping.put("uniqueId", uniqueId);
                    mSocket.emit("on typing", onTyping);
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

                if (charSequence.toString().trim().length() > 0) {
                    sendButton.setEnabled(true);
                } else {
                    sendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    Emitter.Listener receive_message = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(() -> {
                int length = args.length;

                if(length == 0){
                    return;
                }
                //Here i'm getting weird error..................///////run :1 and run: 0
                Log.i(TAG, "run: ");
                Log.i(TAG, "run: " + args.length);
                String username =args[0].toString();
                Log.e(TAG, "call: "+username);

               /* try {
                    JSONObject object = new JSONObject(username);
                    username = object.getString("username");
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

                MessageFormat format = new MessageFormat(null, username, null);
                chatAdapter.add(format);
                messageListView.smoothScrollToPosition(0);
                messageListView.scrollTo(0, chatAdapter.getCount()-1);
                Log.i(TAG, "run: " + username);
            });
        }
    };

    Emitter.Listener receive_all_messages = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(() -> {
                Log.i(TAG, "run: ");
                Log.i(TAG, "run: " + args.length);
                JSONObject data = (JSONObject) args[0];
                Log.e(TAG, "call: "+data );
                String username;
                String message;
                String id;
                try {
                  /*  username = data.getString("username");
                    message = data.getString("message");
                    id = data.getString("uniqueId");

                    Log.i(TAG, "run: " + username + message + id);

                    MessageFormat format = new MessageFormat(id, username, message);
                    Log.i(TAG, "run:4 ");
                    chatAdapter.add(format);
                    Log.i(TAG, "run:5 ");*/

                } catch (Exception e) {
                    return;
                }
            });
        }
    };


}
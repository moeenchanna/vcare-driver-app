package com.easy.vcare.driver.ui.activity;

import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;

import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.R;
import com.easy.vcare.driver.base.BaseActivity;

import de.hdodenhof.circleimageview.CircleImageView;

public class RatingActivity extends BaseActivity {

    private static final String TAG = "RatingActivity";

    private RatingBar mRatingBar;
    private CircleImageView mDriverProfileImg;
    private TextView mDriverNameTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        statusbarTheme();
        initView();
    }

    private void initView() {
        mRatingBar = (RatingBar) findViewById(R.id.ratingBar);
        mDriverProfileImg = (CircleImageView) findViewById(R.id.img_driver_profile);
        mDriverNameTv = (TextView) findViewById(R.id.tv_driver_name);


        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                switch ((int)v)
                {
                    case 1:
                        //Toast.makeText(Rating.this, "Very bad", Toast.LENGTH_SHORT).show();
                        toastCall("1");
                        break;
                    case 2:
                        toastCall("2");
                        break;

                    case 3:
                        toastCall("3");
                        break;
                    case 4:
                        toastCall("4");
                        break;
                    case 5:
                        toastCall("5");
                        break;

                }


            }
        });
    }
}
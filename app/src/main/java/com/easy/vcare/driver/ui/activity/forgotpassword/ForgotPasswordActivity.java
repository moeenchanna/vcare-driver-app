package com.easy.vcare.driver.ui.activity.forgotpassword;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.data.requests.ForgotRequest;
import com.easy.vcare.driver.data.responses.ForgotResponse;
import com.easy.vcare.driver.remote.ApiInterface;
import com.easy.vcare.driver.remote.ServiceGenerator;
import com.easy.vcare.driver.ui.activity.otp.LoginOtpActivity;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {

    private EditText mMobile;
    private Button mBtnsub;
    String number;
    String responseOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initView();
    }

    private void initView() {
        mMobile = (EditText) findViewById(R.id.editphone);
        mBtnsub = (Button) findViewById(R.id.btnsub);
        mBtnsub.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnsub:
                number = mMobile.getText().toString();
                forgotRequest(number);
                // TODO 20/12/30
                break;
            default:
                break;
        }
    }

    private void forgotRequest(String contactNo ) {
        Log.d(TAG, "forgotRequest: ");
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<ForgotResponse> call = apiInterface.getForgotPassword(new ForgotRequest(contactNo));
        call.enqueue(new Callback<ForgotResponse>() {
            @Override
            public void onResponse(Call<ForgotResponse> call, Response<ForgotResponse> response) {
                if (response.isSuccessful()){
                    Log.e(TAG, "onResponse: "+response );

                    assert response.body() != null;

                    Log.e(TAG, "onResponse: "+response.body().getMessage());
                    Log.e(TAG, "onResponse: "+response.body().getStatus());
                    String response_msg = response.body().getMessage();
                    //sendOtpCode(number);


                    responseOtp =String.valueOf(response.body().getData().get(0).getOtp());
                    toastCall(response_msg);
                    snackbarCall(response_msg);

                    if (response.body().getStatus() == 200){
                        Log.e(TAG, "onResponse: "+ "go otp" );
                        mainIntent = new Intent(ForgotPasswordActivity.this, LoginOtpActivity.class);
                        mainIntent.putExtra("number", number);
                        mainIntent.putExtra(responseOtp, responseOtp);
                        mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        ForgotPasswordActivity.this.startActivity(mainIntent);
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    }
                }
            }

            @Override
            public void onFailure(Call<ForgotResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Log.d("error", Objects.requireNonNull(t.getMessage()));
                toastCall("Server Error");
            }
        });

    }
}
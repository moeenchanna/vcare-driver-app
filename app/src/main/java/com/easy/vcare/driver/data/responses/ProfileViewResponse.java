package com.easy.vcare.driver.data.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileViewResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("errors")
    @Expose
    private List<Object> errors = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public List<Object> getErrors() {
        return errors;
    }

    public void setErrors(List<Object> errors) {
        this.errors = errors;
    }

    public class Wallet {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("users_type")
        @Expose
        private String usersType;
        @SerializedName("users_id")
        @Expose
        private Integer usersId;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUsersType() {
            return usersType;
        }

        public void setUsersType(String usersType) {
            this.usersType = usersType;
        }

        public Integer getUsersId() {
            return usersId;
        }

        public void setUsersId(Integer usersId) {
            this.usersId = usersId;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class Data {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("device_id")
        @Expose
        private Object deviceId;
        @SerializedName("vehicle_id")
        @Expose
        private Integer vehicleId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("contact_no")
        @Expose
        private String contactNo;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("otp")
        @Expose
        private String otp;
        @SerializedName("api_token")
        @Expose
        private String apiToken;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("current_lat")
        @Expose
        private String currentLat;
        @SerializedName("current_lng")
        @Expose
        private String currentLng;
        @SerializedName("availability")
        @Expose
        private String availability;
        @SerializedName("license_no")
        @Expose
        private String licenseNo;
        @SerializedName("cnic_no")
        @Expose
        private String cnicNo;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("driver_request_id")
        @Expose
        private Integer driverRequestId;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("subscription_id")
        @Expose
        private Object subscriptionId;
        @SerializedName("platform")
        @Expose
        private Object platform;
        @SerializedName("wallets")
        @Expose
        private List<Wallet> wallets = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Object getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(Object deviceId) {
            this.deviceId = deviceId;
        }

        public Integer getVehicleId() {
            return vehicleId;
        }

        public void setVehicleId(Integer vehicleId) {
            this.vehicleId = vehicleId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getContactNo() {
            return contactNo;
        }

        public void setContactNo(String contactNo) {
            this.contactNo = contactNo;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getApiToken() {
            return apiToken;
        }

        public void setApiToken(String apiToken) {
            this.apiToken = apiToken;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCurrentLat() {
            return currentLat;
        }

        public void setCurrentLat(String currentLat) {
            this.currentLat = currentLat;
        }

        public String getCurrentLng() {
            return currentLng;
        }

        public void setCurrentLng(String currentLng) {
            this.currentLng = currentLng;
        }

        public String getAvailability() {
            return availability;
        }

        public void setAvailability(String availability) {
            this.availability = availability;
        }

        public String getLicenseNo() {
            return licenseNo;
        }

        public void setLicenseNo(String licenseNo) {
            this.licenseNo = licenseNo;
        }

        public String getCnicNo() {
            return cnicNo;
        }

        public void setCnicNo(String cnicNo) {
            this.cnicNo = cnicNo;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getDriverRequestId() {
            return driverRequestId;
        }

        public void setDriverRequestId(Integer driverRequestId) {
            this.driverRequestId = driverRequestId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getSubscriptionId() {
            return subscriptionId;
        }

        public void setSubscriptionId(Object subscriptionId) {
            this.subscriptionId = subscriptionId;
        }

        public Object getPlatform() {
            return platform;
        }

        public void setPlatform(Object platform) {
            this.platform = platform;
        }

        public List<Wallet> getWallets() {
            return wallets;
        }

        public void setWallets(List<Wallet> wallets) {
            this.wallets = wallets;
        }

    }
}
package com.easy.vcare.driver.remote;

import com.easy.vcare.driver.base.BaseActivity;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {

    @NotNull
    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {

        //rewrite the request to add bearer token
        Request newRequest=chain.request().newBuilder()
                .header("Authorization","Bearer "+ BaseActivity.AUTHORIZE_TOKEN)
                .build();

        return chain.proceed(newRequest);
    }
}
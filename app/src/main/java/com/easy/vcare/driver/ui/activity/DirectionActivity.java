package com.easy.vcare.driver.ui.activity;

import android.Manifest;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.RequestResult;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Info;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.R;
import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.helper.LatLngInterpolator;
import com.easy.vcare.driver.helper.MarkerAnimation;
import com.easy.vcare.driver.helper.models.events.BeginJourneyEvent;
import com.easy.vcare.driver.helper.models.events.CurrentJourneyEvent;
import com.easy.vcare.driver.helper.models.events.EndJourneyEvent;
import com.easy.vcare.driver.helper.models.events.JourneyEventBus;
import com.easy.vcare.driver.helper.polylineBased.MapAnimator;
import com.easy.vcare.driver.ui.activity.chat.ChatRoomActivity;
import com.easy.vcare.driver.ui.activity.otp.OtpVerificationActivity;
import com.easy.vcare.driver.untils.Constants;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

public class DirectionActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener {

    private static final String TAG = "DirectionActivity";

    Button arrived,pickup,drop,complete;

    double lat_drive , lng_ddrive;

    public LatLng destination = null;
    public LatLng rider = null;

    Leg leg;
    ArrayList<LatLng> directionPositionList;
    public Marker _pickupMarker, _dropMarker, _rideMarker;
    private Handler handler;
    private static final long DELAY = 500;
    private int index, next;
    private LatLng startPosition, endPosition;
    private float v;

    private FloatingActionButton mFabChat;
    String user_room,user_request_id;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mapstatusbarTheme();
        setContentView(R.layout.activity_direction);

        //Socket connection
        try {
            final IO.Options options = new IO.Options();
            options.transports = Constants.TRANSPORTS;
            Constants.mSocket = IO.socket(Constants.SOCKET_URI, options);
            Log.e(TAG, "Socket Connection Create: ");

        } catch (URISyntaxException e) {
            Log.e(TAG, "URISyntaxException: ", e);
        }

        Constants.mSocket.connect();
        Log.e(TAG, "Socket.connect: ");

        arrived =findViewById(R.id.btn_arrived);
        pickup =findViewById(R.id.btn_pickup);
        drop =findViewById(R.id.btn_drop);
        complete =findViewById(R.id.btn_complete);
        mFabChat =  findViewById(R.id.fab_chat);
        mFabChat.setOnClickListener(this);


        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        assert supportMapFragment != null;
        supportMapFragment.getMapAsync(this);

        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            lat_drive= Double.parseDouble(intent.getStringExtra("start_latitude"));
            lng_ddrive = Double.parseDouble(intent.getStringExtra("start_longitude"));
            user_room = intent.getStringExtra("user_room");
            user_request_id = intent.getStringExtra("user_request_id");

            Log.e(TAG, "user_request_id: "+user_request_id );
            Log.e(TAG, "user_room: "+user_room );
        }

        Log.e(TAG, "lat: " + lat_drive);
        Log.e(TAG, "lng: " + lng_ddrive);
        destination = new LatLng(lat_drive, lng_ddrive);



        new Handler().postDelayed(() -> driverComming(),3000);

        arrived.setOnClickListener(view -> {
            Constants.mSocket.emit("join_room", DRIVER_ID + "driver");


        });

        pickup.setOnClickListener(view -> { });

        drop.setOnClickListener(view -> { });

        complete.setOnClickListener(view -> { });

      /*  *//* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*//*
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                *//* Create an Intent that will start the Menu-Activity. *//*
                mainIntent = new Intent(DirectionActivity.this,FareAmountActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                DirectionActivity.this.startActivity(mainIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

            }
        }, 4000);*/


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        mapStyle();
    }

    private void startCurrentLocationUpdates() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(3000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(DirectionActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                return;
            }
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status)
            return true;
        else {
            if (googleApiAvailability.isUserResolvableError(status))
                Toast.makeText(this, "Please Install google play services to use this application", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Toast.makeText(this, "Permission denied by uses", Toast.LENGTH_SHORT).show();
            else if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                startCurrentLocationUpdates();
        }
    }

    private void animateCamera(@NonNull Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLng)));
    }

    @NonNull
    private CameraPosition getCameraPositionWithBearing(LatLng latLng) {
        return new CameraPosition.Builder().target(latLng).zoom(16).build();
    }

    private void showMarker(@NonNull Location currentLocation) {
        rider = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());


        double latitude = currentLocation.getLatitude();
        double longitude = currentLocation.getLongitude();
        //Log.e(TAG, "latitude: " + latitude);
        // Log.e(TAG, "longitude: " + longitude);
        JSONObject update_location_jsonObject = new JSONObject();
        try {
            update_location_jsonObject.put("driver_id", DRIVER_ID);
            update_location_jsonObject.put("lat", latitude);
            update_location_jsonObject.put("lng", longitude);

            // Log.e(TAG, "send update_location_jsonObject: " + update_location_jsonObject);
            Constants.mSocket.emit("update_location", update_location_jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (currentLocationMarker == null) {
            //Create a new marker
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(rider);
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car));
            markerOptions.rotation(currentLocation.getBearing());
            markerOptions.anchor((float) 0.5, (float) 0.5);
            currentLocationMarker = googleMap.addMarker(markerOptions);
            MarkerAnimation.animateMarkerToGB(currentLocationMarker, rider, new LatLngInterpolator.Spherical());
            //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
        } else {
            //use the previously created marker
            currentLocationMarker.setPosition(rider);
            currentLocationMarker.setRotation(currentLocation.getBearing());
            //googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            MarkerAnimation.animateMarkerToGB(currentLocationMarker, rider, new LatLngInterpolator.Spherical());
        }
    }

    private void driverComming() {

        //  handler.post(mStatusChecker);
        googleMap.clear();

        googleMap.addMarker(new MarkerOptions().position(destination).title("Pick Location"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(rider));
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                .target(googleMap.getCameraPosition().target)
                .zoom(17)
                .bearing(30)
                .tilt(45)
                .build()));


        GoogleDirection.withServerKey(Constants.MAP_KEY)
                .from(this.destination)
                .to(rider)
                .transportMode(TransportMode.DRIVING)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {

                        String status = direction.getStatus();
                        if (status.equals(RequestResult.OK)) {
                            Route route = direction.getRouteList().get(0);
                            leg = route.getLegList().get(0);
                            Info distanceInfo = leg.getDistance();
                            Info durationInfo = leg.getDuration();
                            String distance = distanceInfo.getText();
                            String duration = durationInfo.getText();
                            Log.e(TAG, "distance: " + distance);
                            Log.e(TAG, "duration: " + duration);
                            directionPositionList = leg.getDirectionPoint();

                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            for (LatLng latLng : directionPositionList) {
                                builder.include(latLng);
                            }

                            LatLngBounds bounds = builder.build();
                            CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 2);
                            googleMap.animateCamera(mCameraUpdate);


                            startAnim();

                            _rideMarker = googleMap.addMarker(new MarkerOptions().position(rider)
                                    .flat(true)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car)));
                            handler = new Handler();
                            index = -1;
                            next = 1;
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (index < directionPositionList.size() - 1) {
                                        index++;
                                        next = index + 1;
                                    }
                                    if (index < directionPositionList.size() - 1) {
                                        startPosition = directionPositionList.get(index);
                                        endPosition = directionPositionList.get(next);
                                    }
                                    if (index == 0) {
                                        BeginJourneyEvent beginJourneyEvent = new BeginJourneyEvent();
                                        beginJourneyEvent.setBeginLatLng(startPosition);
                                        JourneyEventBus.getInstance().setOnJourneyBegin(beginJourneyEvent);

                                    }
                                    if (index == directionPositionList.size() - 1) {
                                        EndJourneyEvent endJourneyEvent = new EndJourneyEvent();
                                        endJourneyEvent.setEndJourneyLatLng(new LatLng(directionPositionList.get(index).latitude,
                                                directionPositionList.get(index).longitude));
                                        JourneyEventBus.getInstance().setOnJourneyEnd(endJourneyEvent);
                                    }
                                    ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                                    valueAnimator.setDuration(3000);
                                    valueAnimator.setInterpolator(new LinearInterpolator());
                                    valueAnimator.addUpdateListener(valueAnimator1 -> {
                                        v = valueAnimator1.getAnimatedFraction();
                                        lng_ddrive = v * endPosition.longitude + (1 - v)
                                                * startPosition.longitude;
                                        lat_drive = v * endPosition.latitude + (1 - v)
                                                * startPosition.latitude;
                                        CurrentJourneyEvent currentJourneyEvent = new CurrentJourneyEvent();
                                        currentJourneyEvent.setCurrentLatLng(rider);
                                        JourneyEventBus.getInstance().setOnJourneyUpdate(currentJourneyEvent);
                                        _rideMarker.setPosition(rider);
                                        _rideMarker.setAnchor(0.5f, 0.5f);
                                        _rideMarker.setRotation(getBearing(startPosition, rider));
                                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition
                                                (new CameraPosition.Builder().target(rider)
                                                        .zoom(15.5f).build()));
                                    });
                                    valueAnimator.start();
                                    if (index != directionPositionList.size() - 1) {
                                        handler.postDelayed(this, 3000);
                                    }
                                }
                            }, 3000);


                        } else if (status.equals(RequestResult.NOT_FOUND)) {
                            Toast.makeText(DirectionActivity.this, "No routes exist", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something here
                    }
                });

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (fusedLocationProviderClient != null)
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isGooglePlayServicesAvailable()) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            startCurrentLocationUpdates();


        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fusedLocationProviderClient = null;
        googleMap = null;

    }
    private void startAnim() {
        if (googleMap != null) {
            MapAnimator.getInstance().animateRoute(googleMap, directionPositionList);
        } else {
            Toast.makeText(getApplicationContext(), "Map not ready", Toast.LENGTH_LONG).show();
        }
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private final LocationCallback mLocationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            if (locationResult.getLastLocation() == null)
                return;
            currentLocation = locationResult.getLastLocation();
            if (firstTimeFlag && googleMap != null) {
                animateCamera(currentLocation);
                firstTimeFlag = false;
            }
            showMarker(currentLocation);
        }
    };


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fab_chat:
                // TODO 20/08/11
                Intent intent = new Intent(DirectionActivity.this, ChatRoomActivity.class);
                intent.putExtra("user_request_id", user_request_id);
                intent.putExtra("room", user_room);
                startActivity(intent);

                break;

            default:
                break;
        }


    }
}
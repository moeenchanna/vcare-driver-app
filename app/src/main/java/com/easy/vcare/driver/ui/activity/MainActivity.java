package com.easy.vcare.driver.ui.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.helper.LatLngInterpolator;
import com.easy.vcare.driver.helper.MarkerAnimation;
import com.easy.vcare.driver.ui.activity.chat.ChatRoomActivity;
import com.easy.vcare.driver.untils.Constants;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Arrays;

public class MainActivity extends BaseActivity implements
        OnMapReadyCallback,
        View.OnClickListener {

    private static final String TAG = "MainActivity";

    private ImageView mGps;
    private LabeledSwitch mOnlineStatusToggle;
    Dialog dialog;
    private TextView mCustomerNameTv;
    private TextView mLocationTv;
    private TextView mPayemntMode;
    private TextView mTimer;
    GoogleApiClient mGoogleApiClient;
    private Button mVerifyBtn;
    private Button mRejectBtn;

    LocationRequest mLocationRequest;
    DrawerLayout drawer;
    String PREFS = "MyPrefs";
    SharedPreferences mPrefs;
    public static String pick_latitude;
    public static String pick_longitude;

    // Location classes
    private FusedLocationProviderClient mFusedLocationClient;
    private Location mLastLocation;

    String user_room,user_request_id;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mapstatusbarTheme();
        setContentView(R.layout.activity_main);
        //Socket connection
        try {
            final IO.Options options = new IO.Options();
            options.transports = Constants.TRANSPORTS;
            Constants.mSocket = IO.socket(Constants.SOCKET_URI, options);
            Log.e(TAG, "Socket Connection Create: ");

        } catch (URISyntaxException e) {
            Log.e(TAG, "URISyntaxException: ", e);
        }

        Constants.mSocket.connect();
        Log.e(TAG, "Socket.connect: ");

        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        assert supportMapFragment != null;
        supportMapFragment.getMapAsync(this);

        Constants.mSocket.emit("join_room", DRIVER_ID + "driver");

        Constants.mSocket.on("incoming_ride", incoming_ride);

        Constants.mSocket.on("ride_accepted", ride_accepted);

        JSONObject driver_status_jsonObject = new JSONObject();

        try {
            driver_status_jsonObject.put("driver_id", DRIVER_ID);
            driver_status_jsonObject.put("status", "ONLINE");
            Log.e(TAG, "send driver_status_jsonObject: " + driver_status_jsonObject);

            Constants.mSocket.emit("driver_status", driver_status_jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        initView();
        mPrefs = getSharedPreferences(PREFS, 0);
    }


    private final Emitter.Listener ride_accepted = args -> runOnUiThread(() -> {
        Log.e(TAG, "run: ride_accepted " + Arrays.toString(args));

        JSONObject data = (JSONObject) args[0];

        Log.e(TAG, "ride_accepted: " + data);

        try {
            dialog.dismiss();

            user_room = data.getString("room");
            user_request_id = data.getJSONObject("user_request").getString("id");
            Log.e(TAG, "user_room: "+user_room );
            Log.e(TAG, "user_request_id: "+user_request_id );

            String status = data.getJSONObject("user_request").getString("status");
            String driver_id = data.getJSONObject("user_request").getJSONObject("Driver").getString("id");
            String driver_name = data.getJSONObject("user_request").getJSONObject("Driver").getString("name");
            String start_latitude = data.getJSONObject("user_request").getString("start_latitude");
            String start_longitude = data.getJSONObject("user_request").getString("start_longitude");


            Log.e(TAG, "status: " + status);
            Log.e(TAG, "driver_id: " + driver_id);
            Log.e(TAG, "driver_name: " + driver_name);
            Log.e(TAG, "start_latitude: " + start_latitude);
            Log.e(TAG, "start_longitude: " + start_longitude);

            Constants.mSocket.emit("leave_room", driver_id + "driver");
            Constants.mSocket.emit("join_room", user_room);

            if (status.contains("ACCEPTED")) {

                mainIntent = new Intent(MainActivity.this, DirectionActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                mainIntent.putExtra("start_latitude", start_latitude);
                mainIntent.putExtra("start_longitude", start_longitude);
                mainIntent.putExtra("user_room", user_room);
                mainIntent.putExtra("user_request_id", user_request_id);
                MainActivity.this.startActivity(mainIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    });


    private final Emitter.Listener incoming_ride = args -> runOnUiThread(() -> {

        try {
            Log.e(TAG, "run: incoming_ride " + Arrays.toString(args));

            JSONObject data = (JSONObject) args[0];

            Log.e(TAG, "incoming_ride: " + data);
            String user_request_id = data.getString("id");
            String status = data.getString("status");
            String booking_id = data.getString("booking_id");
            String payment_mode = data.getString("payment_mode");
            String paid = data.getString("paid");
            String start_address = data.getString("start_address");
            String end_address = data.getString("end_address");

            if (status.contains("SEARCHING")) {

                showUpcomingRidingPopup(user_request_id, booking_id, payment_mode, paid, start_address, end_address);
            } else if (status.contains("SEARCHING")) {
                dialog.dismiss();
            }


        } catch (Exception ex) {
            Log.e(TAG, "Exception: ", ex);
        }


    });

    private void initView() {
        mGps = findViewById(R.id.gps);
        mGps.setOnClickListener(this);
        mOnlineStatusToggle = findViewById(R.id.toggle_online_status);

        /*NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/
        mPrefs = getSharedPreferences(PREFS, MODE_PRIVATE);

        Boolean status_value = mPrefs.getBoolean("status", false);
        Log.e(TAG, "getBoolean: " + status_value);
        if (status_value) {
            mOnlineStatusToggle.setOn(true);
            Constants.mSocket.emit("join_room", DRIVER_ID + "driver");


        } else {
            mOnlineStatusToggle.setOn(false);

        }

        mOnlineStatusToggle.setOnToggledListener((toggleableView, isOn) -> {

            if (isOn) {

                if (googleMap != null && currentLocation != null) {
                    animateCamera(currentLocation);

                    toastCall("Online");
                    mPrefs = getSharedPreferences(PREFS, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = mPrefs.edit();
                    editor.putBoolean("status", true);
                    editor.apply();
                    mOnlineStatusToggle.setOn(true);

                } else {

                    toastCall("Offline");
                    dialog.dismiss();
                    mPrefs = getSharedPreferences(PREFS, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = mPrefs.edit();
                    editor.putBoolean("status", false);
                    editor.apply();
                    mOnlineStatusToggle.setOn(false);

                }
            }
        });



    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        mapStyle();
    }

    private void startCurrentLocationUpdates() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(3000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                return;
            }
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status)
            return true;
        else {
            if (googleApiAvailability.isUserResolvableError(status))
                Toast.makeText(this, "Please Install google play services to use this application", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Toast.makeText(this, "Permission denied by uses", Toast.LENGTH_SHORT).show();
            else if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                startCurrentLocationUpdates();
        }
    }

    private void animateCamera(@NonNull Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLng)));
    }

    @NonNull
    private CameraPosition getCameraPositionWithBearing(LatLng latLng) {
        return new CameraPosition.Builder().target(latLng).zoom(16).build();
    }

    private void showMarker(@NonNull Location currentLocation) {
        LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());


        double latitude = currentLocation.getLatitude();
        double longitude = currentLocation.getLongitude();
        //Log.e(TAG, "latitude: " + latitude);
        // Log.e(TAG, "longitude: " + longitude);
        JSONObject update_location_jsonObject = new JSONObject();
        try {
            update_location_jsonObject.put("driver_id", DRIVER_ID);
            update_location_jsonObject.put("lat", latitude);
            update_location_jsonObject.put("lng", longitude);

            // Log.e(TAG, "send update_location_jsonObject: " + update_location_jsonObject);
            Constants.mSocket.emit("update_location", update_location_jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

      /*  if (currentLocationMarker == null)
            currentLocationMarker = googleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker()).position(latLng));
        else
            MarkerAnimation.animateMarkerToGB(currentLocationMarker, latLng, new LatLngInterpolator.Spherical());*/


        if (currentLocationMarker == null) {
            //Create a new marker
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car));
            markerOptions.rotation(currentLocation.getBearing());
            markerOptions.anchor((float) 0.5, (float) 0.5);
            currentLocationMarker = googleMap.addMarker(markerOptions);
            MarkerAnimation.animateMarkerToGB(currentLocationMarker, latLng, new LatLngInterpolator.Spherical());
            //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
        } else {
            //use the previously created marker
            currentLocationMarker.setPosition(latLng);
            currentLocationMarker.setRotation(currentLocation.getBearing());
            //googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            MarkerAnimation.animateMarkerToGB(currentLocationMarker, latLng, new LatLngInterpolator.Spherical());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (fusedLocationProviderClient != null)
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isGooglePlayServicesAvailable()) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            startCurrentLocationUpdates();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fusedLocationProviderClient = null;
        googleMap = null;

        if (dialog != null && dialog.isShowing()) {
            dialog.cancel();
        }
    }

    private final LocationCallback mLocationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            if (locationResult.getLastLocation() == null)
                return;
            currentLocation = locationResult.getLastLocation();
            if (firstTimeFlag && googleMap != null) {
                animateCamera(currentLocation);
                firstTimeFlag = false;
            }
            showMarker(currentLocation);
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gps:
                // TODO 20/08/11
                if (googleMap != null && currentLocation != null) {
                    animateCamera(currentLocation);
                }
                break;

            default:
                break;
        }
    }

    private void showUpcomingRidingPopup(String user_request_id, String booking_id, String payment_mode, String paid, String start_address, String end_address) {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_booking_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        dialog.show();


        mCustomerNameTv = dialog.findViewById(R.id.tv_customer_name);
        mLocationTv = dialog.findViewById(R.id.tv_location);
        mPayemntMode = dialog.findViewById(R.id.tv_payment_mode);
        mVerifyBtn = dialog.findViewById(R.id.btn_verify);
        mRejectBtn = dialog.findViewById(R.id.btn_reject);
        mTimer = dialog.findViewById(R.id.tv_timer);

        mCustomerNameTv.setText("Not Available");
        mLocationTv.setText("Pick Location: " + start_address + "\n" + "\n" + "Drop Location: " + end_address);
        mPayemntMode.setText(payment_mode);

        initCountDownTimer();

        mVerifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toastCall("Accept Ride");


                JSONObject ride_accepted_jsonObject = new JSONObject();

                try {
                    ride_accepted_jsonObject.put("user_request_id", user_request_id);
                    ride_accepted_jsonObject.put("driver_id", DRIVER_ID);
                    Log.e(TAG, "send ride_accepted_jsonObject: " + ride_accepted_jsonObject);
                    Constants.mSocket.emit("accept_ride", ride_accepted_jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        mRejectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toastCall("Reject");
                dialog.dismiss();
            }
        });
    }

    public void initCountDownTimer() {
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                mTimer.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {

                mTimer.setText("0");
                dialog.dismiss();
            }
        }.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            intentTransition();
        }
    }


}




package com.easy.vcare.driver.ui.activity.ride;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.adapter.RideHistoryAdapter;
import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.data.responses.RideViewResponse;
import com.easy.vcare.driver.remote.ApiInterface;
import com.easy.vcare.driver.remote.ErrorUtils;
import com.easy.vcare.driver.remote.RetrofitClient;
import com.easy.vcare.driver.ui.activity.MainActivity;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RideHistoryActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView mRecyclerviewRide;

    RideHistoryAdapter rideHistoryAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_history);

            initView();
        mainIntent = getIntent();
        extras = mainIntent.getExtras();
        assert extras != null;
        AUTHORIZE_TOKEN = extras.getString("token");

        Log.e(TAG, "AUTHORIZE_TOKEN: " + AUTHORIZE_TOKEN);

    }

    private void initView() {
        mTitle = (TextView) findViewById(R.id.title);
        mRecyclerviewRide = (RecyclerView) findViewById(R.id.ride_recyclerview);
        mTitle.setText("Ride History");

        mRecyclerviewRide.setHasFixedSize(true);
        //set a vertical layout so the list is displayed top down
        final LinearLayoutManager layoutManager = new LinearLayoutManager(RideHistoryActivity.this, LinearLayoutManager.VERTICAL, false);
        mRecyclerviewRide.setLayoutManager(layoutManager);

     /*   RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RideHistoryActivity.this);


        mRecyclerviewRide.setLayoutManager(mLayoutManager);
        mRecyclerviewRide.setAdapter(rideHistoryAdapter);
        mRecyclerviewRide.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(mRecyclerviewRide, false);
        mRecyclerviewRide.setHasFixedSize(true);
        mRecyclerviewRide.setItemViewCacheSize(20);
        mRecyclerviewRide.setDrawingCacheEnabled(true);
        mRecyclerviewRide.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);*/


        showProgressDialog("Loading Services", "Please wait...");
        getRideData();
    }

    @SuppressLint("LongLogTag")
    private void getRideData() {

        Log.e(TAG, "getWalletData: ");

        ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);
        Call<RideViewResponse> call = apiInterface.getRideHistory();

        Log.e(TAG, "registerRequest: " + call.toString());

        call.enqueue(new Callback<RideViewResponse>() {
            @Override
            public void onResponse(@NotNull Call<RideViewResponse> call, @NotNull Response<RideViewResponse> response) {
                Log.e(TAG, "onResponse: " + response);

                try {
                    if (response.isSuccessful()) {
                        hideProgressDialog();
                        Log.e(TAG, "onResponse: " + response);

                        assert response.body() != null;

                        Log.e(TAG, "onResponse: " + response.body().getMessage());
                        Log.e(TAG, "onResponse: " + response.body().getStatus());

                        if (response.body().getStatus() == 200) {
                            hideProgressDialog();
                            Log.e(TAG, "onResponse: " + response);

                            RideViewResponse rideHistoryResponse = response.body();
                            if (rideHistoryResponse != null) {

                                String response_msg = rideHistoryResponse.getMessage();
                                Log.e(TAG, "onResponse: " + response_msg);

                                List<RideViewResponse.Datum> rideHistoryResponses;
                                rideHistoryResponses = response.body().getData();

                                rideHistoryAdapter = new RideHistoryAdapter(rideHistoryResponses, RideHistoryActivity.this);
                                mRecyclerviewRide.setAdapter(rideHistoryAdapter);
                                rideHistoryAdapter.setRideList(rideHistoryResponses);


                                toastCall(response_msg);
                                snackbarCall(response_msg);


                            }

                        } else {
                            if (response.errorBody() != null) {

                                Gson gson = new Gson();
                                ErrorUtils message = gson.fromJson(response.errorBody().charStream(), ErrorUtils.class);

                                Log.e(TAG, "Error Response: " + message.getStatus());
                                Log.e(TAG, "Error Response: " + message.getMessage());


                                Toast.makeText(RideHistoryActivity.this, message.getMessage(), Toast.LENGTH_SHORT).show();


                            }
                        }
                    }
                } catch (Exception e) {
                    hideProgressDialog();
                    Log.e(TAG, "catch: ", e);

                }

            }


            @Override
            public void onFailure(Call<RideViewResponse> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "onFailure: ", t);
                Log.d("error", Objects.requireNonNull(t.getMessage()));
                toastCall("Server Error");
            }
        });

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                // TODO 20/07/21
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        mainIntent = new Intent(RideHistoryActivity.this, MainActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
        RideHistoryActivity.this.startActivity(mainIntent);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
package com.easy.vcare.driver.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.R;
import com.easy.vcare.driver.base.BaseActivity;

public class FareAmountActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "FareAmountActivity";

    private Button mFareamountButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        statusbarTheme();
        setContentView(R.layout.activity_fareamount);
        initView();
    }

    private void initView() {
        mFareamountButton = (Button) findViewById(R.id.fareamount_button);
        mFareamountButton.setOnClickListener(this);
    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fareamount_button:
                // TODO 20/06/26
                mainIntent = new Intent(FareAmountActivity.this,RatingActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                FareAmountActivity.this.startActivity(mainIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intentTransition();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
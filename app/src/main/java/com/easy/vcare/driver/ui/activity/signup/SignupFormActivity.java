package com.easy.vcare.driver.ui.activity.signup;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.ui.fragment.SignupData;

public class SignupFormActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_form);

        SignupData blankFragment = new SignupData();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,blankFragment).commit();
    }
}
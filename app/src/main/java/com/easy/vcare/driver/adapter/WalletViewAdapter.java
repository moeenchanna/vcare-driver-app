package com.easy.vcare.driver.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.data.responses.WalletViewResponse;

import java.util.List;

import static androidx.constraintlayout.motion.utils.Oscillator.TAG;

public class WalletViewAdapter extends RecyclerView.Adapter<WalletViewAdapter.WalletView> {
    List<WalletViewResponse.Datum> walletViewResponses;
    Context mContext;

    public WalletViewAdapter(List<WalletViewResponse.Datum> walletViewResponses, Context mContext) {
        this.walletViewResponses = walletViewResponses;
        this.mContext = mContext;
    }

    public void setWalletList(List<WalletViewResponse.Datum> walletViewResponses) {
        this.walletViewResponses = walletViewResponses;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public WalletViewAdapter.WalletView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.recyler_walletview,parent,false);
        return new WalletViewAdapter.WalletView(view);    }

    @Override
    public void onBindViewHolder(@NonNull WalletViewAdapter.WalletView holder, int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        holder.amount.setText("Rs "+walletViewResponses.get(position).getAmount());
        holder.comment.setText(walletViewResponses.get(position).getComment());



    }

    @Override
    public int getItemCount() {
        if(walletViewResponses != null){
            return walletViewResponses.size();
        }
        return 0;
    }

    public class WalletView extends RecyclerView.ViewHolder {
        TextView amount, comment;

        public WalletView(@NonNull View itemView) {
            super(itemView);
            amount = itemView.findViewById(R.id.txt_amount);
            comment = itemView.findViewById(R.id.cmt);


        }
    }
}
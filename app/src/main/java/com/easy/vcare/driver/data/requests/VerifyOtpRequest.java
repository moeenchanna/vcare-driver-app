package com.easy.vcare.driver.data.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyOtpRequest {
    @SerializedName("contact_no")
    @Expose
    private String contactNo;

    public VerifyOtpRequest(String contactNo, String otp) {
        this.contactNo = contactNo;
        this.otp = otp;
    }

    @SerializedName("otp")
    @Expose
    private String otp;

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

}
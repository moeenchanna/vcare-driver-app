package com.easy.vcare.driver.data.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgotRequest {
    public ForgotRequest(String contactNo) {
        this.contactNo = contactNo;
    }

    @SerializedName("contact_no")
    @Expose
    private String contactNo;

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

}

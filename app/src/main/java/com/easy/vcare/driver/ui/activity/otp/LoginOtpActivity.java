package com.easy.vcare.driver.ui.activity.otp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.data.requests.SendOtpRequest;
import com.easy.vcare.driver.data.requests.VerifyOtpRequest;
import com.easy.vcare.driver.data.responses.SendOtpResponse;
import com.easy.vcare.driver.data.responses.VerifyOtpResponse;
import com.easy.vcare.driver.remote.ApiInterface;
import com.easy.vcare.driver.remote.ErrorUtils;
import com.easy.vcare.driver.remote.RetrofitClient;
import com.easy.vcare.driver.remote.ServiceGenerator;
import com.easy.vcare.driver.ui.activity.MainActivity;
import com.easy.vcare.driver.ui.activity.WalletActivity;
import com.easy.vcare.driver.ui.activity.ride.RideHistoryActivity;
import com.easy.vcare.driver.ui.activity.signup.SignupFormActivity;
import com.goodiebag.pinview.Pinview;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginOtpActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "OtpVerificationActivity";
    String phone;
    String  userOtp;
    private TextView mButtonResend;
    private Button mButtonVerify;
    private Pinview mPinview;
    private TextView mNumberTv;
    String responseOtp;

    String  password, number;

    Activity context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate: ");
        statusbarTheme();
        setContentView(R.layout.activity_login_otp);



        initView();
    }
    private void initView() {
        mButtonResend = (TextView) findViewById(R.id.resend_button);
        mButtonResend.setOnClickListener(this);
        mButtonVerify = (Button) findViewById(R.id.verify_button);
        mButtonVerify.setOnClickListener(this);
        mPinview = new Pinview(this);
        mPinview = (Pinview) findViewById(R.id.pinview);
        mNumberTv = (TextView) findViewById(R.id.tv_number);


        mainIntent = getIntent();
        extras = mainIntent.getExtras();
        assert extras != null;
        phone = extras.getString("number");
        mNumberTv.setText("Verification code has been sent to " + phone);


        sendOtpCode(phone);

        mPinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                //Make api calls here or what not
                //toastCall(pinview.getValue());
                userOtp = mPinview.getValue();
                Log.e(TAG, "pin no: " + userOtp);
                checkVerificationOTP(userOtp);

            }
        });

    }

    private void sendOtpCode(String phone) {

        ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);
        Call<SendOtpResponse> call = apiInterface.sendOTP(new SendOtpRequest(phone));
        call.enqueue(new Callback<SendOtpResponse>() {


            @Override
            public void onResponse(@NotNull Call<SendOtpResponse> call, @NotNull Response<SendOtpResponse> response) {

                Log.e(TAG, "onResponse: " + response);

                if (response.isSuccessful() && response.body() != null) {

                    Log.e(TAG, "onResponse: " + response);

                    assert response.body() != null;

                    Log.e(TAG, "onResponse: " + response.body().getMessage());
                    Log.e(TAG, "onResponse: " + response.body().getStatus());

                    if (response.body().getStatus() == 200) {

                        Log.e(TAG, "onResponse: " + response);

                        SendOtpResponse sendOtpResponse = response.body();

                        if (sendOtpResponse != null) {

                            //responseOtp = String.valueOf(sendOtpResponse.getData());
                            responseOtp = "1234";


                            String response_msg = sendOtpResponse.getMessage();
                            Log.e(TAG, "onResponse: " + response_msg);

                            toastCall(response_msg);
                            snackbarCall(response_msg);
                        }
                    } else {
                        if (response.errorBody() != null) {

                            Gson gson = new Gson();
                            ErrorUtils message = gson.fromJson(response.errorBody().charStream(), ErrorUtils.class);

                            Log.e(TAG, "Error Response: " + message.getStatus());
                            Log.e(TAG, "Error Response: " + message.getMessage());


                            Toast.makeText(LoginOtpActivity.this, message.getMessage(), Toast.LENGTH_SHORT).show();


                        }
                    }

                }
            }

            @Override
            public void onFailure(@NotNull Call<SendOtpResponse> call, @NotNull Throwable t) {

            }
        });
    }

    public void checkVerificationOTP(String otpCODE) {
        if (otpCODE.equals(responseOtp)) {
            Log.d(TAG, "otpCODE: " + otpCODE);
            Log.d(TAG, "getcode: " + responseOtp);
            Log.e(TAG, "Match: ");

            hideKeyboardwithoutPopulate(context);
            showProgressDialog("Loading", "Please wait...");
            verifyOtpLogin(otpCODE, phone);
            // Set up progress before call
            //showProgressDialog("Authentication", "Please wait...");
            // registerRequest(name, email, password, contact);
            /*mainIntent = new Intent(LoginOtpActivity.this, RideHistoryActivity.class);
            mainIntent.putExtra("number", number);
            mainIntent.putExtra("token", number);

            mainIntent.putExtra("password", password);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
            LoginOtpActivity.this.startActivity(mainIntent);*/

        } else {
            Log.d(TAG, "otpCODE: " + otpCODE);
            Log.d(TAG, "getcode: " + responseOtp);
            //toastCall("Invalid code. Please enter valid code.");
            snackbarCall("Invalid code. Please enter valid code.");

        }

    }
    private void verifyOtpLogin(String phone, String otpCODE) {
        Log.e(TAG, "verifyOtpLogin: " );

        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<VerifyOtpResponse> call = apiInterface.getLoginVerify(new VerifyOtpRequest(otpCODE, phone));
        call.enqueue(new Callback<VerifyOtpResponse>() {

            @Override
            public void onResponse(@NotNull Call<VerifyOtpResponse> call, @NotNull Response<VerifyOtpResponse> response) {

                if (response.isSuccessful() && response.body() != null) {

                    hideProgressDialog();

                    Log.e(TAG, "onResponse: " + response);

                    assert response.body() != null;

                    Log.e(TAG, "onResponse: " + response.body().getMessage());
                    Log.e(TAG, "onResponse: " + response.body().getStatus());

                    if (response.body().getStatus() == 200) {

                        Log.e(TAG, "onResponse: " + response);

                        VerifyOtpResponse verifyOtpResponse = response.body();

                        if (verifyOtpResponse != null) {

                            String response_msg = verifyOtpResponse.getMessage();
                            Log.e(TAG, "onResponse: " + response_msg);

                            String token = verifyOtpResponse.getData().get(0).getToken();
                         /*   String name = verifyOtpResponse.getData().get(0).getName();
                            String email = verifyOtpResponse.getData().get(0).getEmail();
                            String mobile = verifyOtpResponse.getData().get(0).getMobile();
*/
                            //Save Token
                            //Log.e(TAG, "name: " + name);
                            Log.e(TAG, "auth token: " + token);

                            toastCall(response_msg);
                            snackbarCall(response_msg);

                            mainIntent = new Intent(context, WalletActivity.class);
                            mainIntent.putExtra("token", token);
                            mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(mainIntent);
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                            finish();
                        }
                    } else {
                        if (response.errorBody() != null) {

                            Gson gson = new Gson();
                            ErrorUtils message = gson.fromJson(response.errorBody().charStream(), ErrorUtils.class);

                            Log.e(TAG, "Error Response: " + message.getStatus());
                            Log.e(TAG, "Error Response: " + message.getMessage());


                            Toast.makeText(LoginOtpActivity.this, message.getMessage(), Toast.LENGTH_SHORT).show();


                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<VerifyOtpResponse> call, @NotNull Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "onFailure: ", t);
                Log.d("error", Objects.requireNonNull(t.getMessage()));
                toastCall("Server Error");

            }
        });


    }
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.resend_button:
                // TODO 20/06/
                hideKeyboardwithoutPopulate(context);

                toastCall("Resend");
                mPinview.setValue("");
                sendOtpCode(phone);


                break;
            case R.id.verify_button:
                // TODO 20/06/26

                if (mPinview.getValue().isEmpty() || mPinview.getValue().length() < 4) {


                    toastCall("Please enter valid OTP Code");
                    snackbarCall("Please enter valid OTP Code");

                    return;
                } else {

                    userOtp = mPinview.getValue();
                    Log.e(TAG, "userOtp no: " + userOtp);
                    checkVerificationOTP(userOtp);
                }

                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intentTransition();
    }





}
package com.easy.vcare.driver.ui.activity.signup;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.data.requests.SignupRequest;
import com.easy.vcare.driver.data.responses.SignupResponse;
import com.easy.vcare.driver.remote.ApiInterface;
import com.easy.vcare.driver.remote.ErrorUtils;
import com.easy.vcare.driver.remote.ServiceGenerator;
import com.easy.vcare.driver.ui.activity.otp.OtpVerificationActivity;
import com.google.gson.Gson;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "RegisterActivity";
    private EditText mNameTxt;
    private EditText mEmailTxt;
    private EditText mPasswordTxt;
    private EditText mContactTxt;
    private ImageView mButtonNext;
    private TextView mTextView;
    private TextView mTextView1;
    private TextView mTextView7;
    private TextView mTextView3;
    private EditText mConformPasswordTxt;
    private TextView mTextView4;
    private TextView mTextView9;
    private EditText mAddressTxt;
    private TextView mTextView2;
    private EditText mLicenseTxt;
    private TextView mTextView5;
    private EditText mCnicTxt;
    private TextView mTextView0;
    private EditText mDeviceTxt;
    private TextView mTextView12;
    private EditText mPlatformTxt;
    private TextView mTextView14;
    private EditText mBranchCodeTxt;
    private TextView mTextView19;
    private EditText mAccountNumTxt;
    private TextView mTextView17;
    private EditText mBankNumTxt;
    private TextView mTextView16;
    private EditText mAccountHolderTxt;
    private TextView mTextView15;
    private EditText mIbanTxt;
    private TextView mTextView13;
    private EditText mMakeTxt;
    private TextView mTextView100;
    private EditText mModelIdTxt;
    private TextView mTextView66;
    private EditText mColourIdTxt;
    private TextView mTextView36;
    private EditText mMileageIdTxt;
    private TextView mTextView96;
    private EditText mNumberPlateTxt;
    private TextView mTextView06;
    private EditText mRegistrarTxt;
    private TextView mTextView60;
    private EditText mNotesTxt;
    private LinearLayout mUpLayoutSign;
    private TextView mTextView160;
    private EditText mYearIdTxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();


    }

    private void initView() {
        mNameTxt = (EditText) findViewById(R.id.txt_name);
        mEmailTxt = (EditText) findViewById(R.id.txt_email);
        mPasswordTxt = (EditText) findViewById(R.id.txt_password);
        mContactTxt = (EditText) findViewById(R.id.txt_contact);
        mButtonNext = (ImageView) findViewById(R.id.fab);
        mButtonNext.setOnClickListener(this);
        mTextView = (TextView) findViewById(R.id.textView);
        mTextView1 = (TextView) findViewById(R.id.textView1);
        mTextView7 = (TextView) findViewById(R.id.textView7);
        mTextView3 = (TextView) findViewById(R.id.textView3);
        mConformPasswordTxt = (EditText) findViewById(R.id.txt_conform_password);
        mTextView4 = (TextView) findViewById(R.id.textView4);
        mTextView9 = (TextView) findViewById(R.id.textView9);
        mAddressTxt = (EditText) findViewById(R.id.txt_address);
        mTextView2 = (TextView) findViewById(R.id.textView2);
        mLicenseTxt = (EditText) findViewById(R.id.txt_license);
        mTextView5 = (TextView) findViewById(R.id.textView5);
        mCnicTxt = (EditText) findViewById(R.id.txt_cnic);
        mTextView0 = (TextView) findViewById(R.id.textView0);
        mDeviceTxt = (EditText) findViewById(R.id.txt_device);
        mTextView12 = (TextView) findViewById(R.id.textView12);
        mPlatformTxt = (EditText) findViewById(R.id.txt_platform);
        mTextView14 = (TextView) findViewById(R.id.textView14);
        mBranchCodeTxt = (EditText) findViewById(R.id.txt_branch_code);
        mTextView19 = (TextView) findViewById(R.id.textView19);
        mAccountNumTxt = (EditText) findViewById(R.id.txt_account_num);
        mTextView17 = (TextView) findViewById(R.id.textView17);
        mBankNumTxt = (EditText) findViewById(R.id.txt_bank_num);
        mTextView16 = (TextView) findViewById(R.id.textView16);
        mAccountHolderTxt = (EditText) findViewById(R.id.txt_account_holder);
        mTextView15 = (TextView) findViewById(R.id.textView15);
        mIbanTxt = (EditText) findViewById(R.id.txt_iban);
        mTextView13 = (TextView) findViewById(R.id.textView13);
        mMakeTxt = (EditText) findViewById(R.id.txt_make);
        mTextView100 = (TextView) findViewById(R.id.textView100);
        mModelIdTxt = (EditText) findViewById(R.id.txt_model_id);
        mTextView66 = (TextView) findViewById(R.id.textView66);
        mColourIdTxt = (EditText) findViewById(R.id.txt_colour_id);
        mTextView36 = (TextView) findViewById(R.id.textView36);
        mMileageIdTxt = (EditText) findViewById(R.id.txt_mileage_id);
        mTextView96 = (TextView) findViewById(R.id.textView96);
        mNumberPlateTxt = (EditText) findViewById(R.id.txt_number_plate);
        mTextView06 = (TextView) findViewById(R.id.textView06);
        mRegistrarTxt = (EditText) findViewById(R.id.txt_registrar);
        mTextView60 = (TextView) findViewById(R.id.textView60);
        mNotesTxt = (EditText) findViewById(R.id.txt_notes);
        mUpLayoutSign = (LinearLayout) findViewById(R.id.sign_up_layout);
        mTextView160 = (TextView) findViewById(R.id.textView160);
        mYearIdTxt = (EditText) findViewById(R.id.txt_year_id);
    }


    /*  private void register() {
          name = mname.getText().toString().trim();
          email = memail.getText().toString().trim();
          password = mpassword.getText().toString().trim();
          conformPassword = mconformPassword.getText().toString().trim();
          contact = mcontact.getText().toString().trim();
          address = maddress.getText().toString().trim();
          license = mlicense.getText().toString().trim();
          cnic = mcnic.getText().toString().trim();

          if (!validate()) {

          } else {

              Intent i = new Intent(RegisterActivity.this, OtpVerificationActivity.class);
              i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
              RegisterActivity.this.startActivity(i);
              overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
          }
      }


      private boolean validate() {
          boolean valid = true;
          if (name.isEmpty() || name.length() > 32) {
              mname.setError("Please enter your valid name");
              valid = false;
          }
          if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
              memail.setError("Please enter your valid Email");
              valid = false;
          }
          if (password.isEmpty()) {
              mpassword.setError("Please enter your valid Password");
              valid = false;
          }
          if (conformPassword.isEmpty()) {
              mconformPassword.setError("Please enter your valid Conform Password");
              valid = false;
          }
          if (contact.isEmpty()) {
              mcontact.setError("Please enter your valid Phone");
              valid = false;
          }
          if (address.isEmpty()) {
              maddress.setError("Please enter your valid Address");
              valid = false;
          }
          if (license.isEmpty()) {
              mlicense.setError("Please enter your valid License num");
              valid = false;
          }
          if (cnic.isEmpty()) {
              mcnic.setError("Please enter your valid Cnic num");
              valid = false;
          }
          return valid;
      }

  */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                // TODO 20/09/26
                //registerRequest();
                Log.e(TAG, " call registerRequest: ");

                String name = mNameTxt.getText().toString();
                String email = mEmailTxt.getText().toString();
                String password = mPasswordTxt.getText().toString();
                String conformPassword = mConformPasswordTxt.getText().toString().trim();

                String number = mContactTxt.getText().toString();
                String address = mAddressTxt.getText().toString();
                String license = mLicenseTxt.getText().toString();
                String cnic = mCnicTxt.getText().toString();
                String deviceid = mDeviceTxt.getText().toString();
                String platform = mPlatformTxt.getText().toString();
                String branch_code = mBranchCodeTxt.getText().toString();
                String account_number = mAccountNumTxt.getText().toString();
                String bank_name = mBankNumTxt.getText().toString();
                String account_holder_name = mAccountHolderTxt.getText().toString();
                String iban = mIbanTxt.getText().toString();
                String make_id = mMakeTxt.getText().toString();
                String model_id = mModelIdTxt.getText().toString();
                String year_id = mYearIdTxt.getText().toString();
                String color_id = mColourIdTxt.getText().toString();
                String mileage_id = mMileageIdTxt.getText().toString();
                String number_plate = mNumberPlateTxt.getText().toString();
                String registrar = mRegistrarTxt.getText().toString();
                String notes = mNotesTxt.getText().toString();

              /*  postRegistration(name,email,password,conformPassword,number,address,license,cnic,deviceid,platform,branch_code,account_number,bank_name,account_holder_name,iban,make_id,
                        model_id,year_id ,color_id,mileage_id,number_plate,registrar,notes);*/

                mainIntent = new Intent(RegisterActivity.this, OtpVerificationActivity.class);
                mainIntent.putExtra("name", name);
                mainIntent.putExtra("email", email);
                mainIntent.putExtra("password", password);
                mainIntent.putExtra("conformPassword", conformPassword);

                mainIntent.putExtra("number", number);
                mainIntent.putExtra("address", address);
                mainIntent.putExtra("license", license);
                mainIntent.putExtra("cnic", cnic);
                mainIntent.putExtra("deviceid", deviceid);
                mainIntent.putExtra("platform", platform);
                mainIntent.putExtra("branch_code", branch_code);
                mainIntent.putExtra("account_number", account_number);
                mainIntent.putExtra("bank_name", bank_name);
                mainIntent.putExtra("account_holder_name", account_holder_name);
                mainIntent.putExtra("iban", iban);
                mainIntent.putExtra("make_id", make_id);
                mainIntent.putExtra("model_id", model_id);
                mainIntent.putExtra("year_id", year_id);
                mainIntent.putExtra("color_id", color_id);
                mainIntent.putExtra("mileage_id", mileage_id);
                mainIntent.putExtra("number_plate", number_plate);
                mainIntent.putExtra("registrar", registrar);
                mainIntent.putExtra("notes", notes);






                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                RegisterActivity.this.startActivity(mainIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                break;
            default:
                break;
        }
    }

    private void postRegistration(String name, String email, String password, String conformPassword, String number, String address, String license, String cnic, String deviceid, String platform, String branch_code, String account_number, String bank_name, String account_holder_name, String iban, String make_id, String model_id, String year_id, String color_id, String mileage_id, String number_plate, String registrar, String notes) {
        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);

        Call<SignupResponse> call = apiInterface.getRegister(new SignupRequest(name, email, password, conformPassword,number,address,license,cnic,platform, deviceid,branch_code,account_number,bank_name,account_holder_name,iban,make_id,model_id,year_id,color_id,mileage_id,number_plate,registrar,notes));
        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                Log.e(TAG, "onResponse: " + response);

                if (response.isSuccessful() && response.body() != null) {
                    Log.e(TAG, "onResponse: " + response);

                    assert response.body() != null;

                    Log.e(TAG, "onResponse: " + response.body().getMessage());
                    Log.e(TAG, "onResponse: " + response.body().getStatus());

                    if (response.body().getStatus() == 200) {
                        Log.e(TAG, "onResponse: " + response);

                        SignupResponse signupResponse = response.body();

                        if (signupResponse != null) {

                            String response_msg = signupResponse.getMessage();

                            //String token = signupResponse.getData().

                            //Save Token
                            Log.e(TAG, "name: " + name);
                            // Log.e(TAG, "auth token: " + token);


                            toastCall(response_msg);
                            snackbarCall(response_msg);
                        }
                    }
                } else {
                    if (response.errorBody() != null) {

                        Gson gson = new Gson();
                        ErrorUtils message = gson.fromJson(response.errorBody().charStream(), ErrorUtils.class);

                        Log.e(TAG, "Error Response: " + message.getStatus());
                        Log.e(TAG, "Error Response: " + message.getMessage());


                        Toast.makeText(RegisterActivity.this, message.getMessage(), Toast.LENGTH_SHORT).show();


                    }
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Log.d("error", Objects.requireNonNull(t.getMessage()));
                toastCall("Server Error");
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
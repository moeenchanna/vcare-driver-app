package com.easy.vcare.driver.data.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignupRequest{


    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("password_confirmation")
    @Expose
    private String passwordConfirmation;
    @SerializedName("contact_no")
    @Expose
    private String contactNo;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("license_no")
    @Expose
    private String licenseNo;
    @SerializedName("cnic_no")
    @Expose
    private String cnicNo;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("platform")
    @Expose
    private String platform;
    @SerializedName("branch_code")
    @Expose
    private String branchCode;
    @SerializedName("account_number")
    @Expose
    private String accountNumber;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("account_holder_name")
    @Expose
    private String accountHolderName;
    @SerializedName("iban")
    @Expose
    private String iban;
    @SerializedName("make_id")
    @Expose
    private String makeId;
    @SerializedName("model_id")
    @Expose
    private String modelId;
    @SerializedName("year_id")
    @Expose
    private String yearId;
    @SerializedName("color_id")
    @Expose
    private String colorId;
    @SerializedName("mileage_id")
    @Expose
    private String mileageId;
    @SerializedName("number_plate")
    @Expose
    private String numberPlate;
    @SerializedName("registrar")
    @Expose
    private String registrar;

    public SignupRequest(String name, String email, String password, String passwordConfirmation, String contactNo, String address, String licenseNo, String cnicNo, String deviceId, String platform, String branchCode, String accountNumber, String bankName, String accountHolderName, String iban, String makeId, String modelId, String yearId, String colorId, String mileageId, String numberPlate, String registrar, String notes) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.contactNo = contactNo;
        this.address = address;
        this.licenseNo = licenseNo;
        this.cnicNo = cnicNo;
        this.deviceId = deviceId;
        this.platform = platform;
        this.branchCode = branchCode;
        this.accountNumber = accountNumber;
        this.bankName = bankName;
        this.accountHolderName = accountHolderName;
        this.iban = iban;
        this.makeId = makeId;
        this.modelId = modelId;
        this.yearId = yearId;
        this.colorId = colorId;
        this.mileageId = mileageId;
        this.numberPlate = numberPlate;
        this.registrar = registrar;
        this.notes = notes;
    }

    @SerializedName("notes")
    @Expose
    private String notes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public String getCnicNo() {
        return cnicNo;
    }

    public void setCnicNo(String cnicNo) {
        this.cnicNo = cnicNo;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getMakeId() {
        return makeId;
    }

    public void setMakeId(String makeId) {
        this.makeId = makeId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getYearId() {
        return yearId;
    }

    public void setYearId(String yearId) {
        this.yearId = yearId;
    }

    public String getColorId() {
        return colorId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public String getMileageId() {
        return mileageId;
    }

    public void setMileageId(String mileageId) {
        this.mileageId = mileageId;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
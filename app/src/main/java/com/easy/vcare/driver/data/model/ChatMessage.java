package com.easy.vcare.driver.data.model;

public class ChatMessage {
    public static final int TYPE_MESSAGE = 0;
    public static final int TYPE_LOG = 1;
    public static final int TYPE_ACTION = 2;

    private int mType;
    private String user_request_id;
    private String type;
    private String message;
    private String attachment;
    private String room;

    private ChatMessage() {}

    public int getType() {
        return mType;
    }

    public String getUser_request_id() {
        return user_request_id;
    }

    public void setUser_request_id(String user_request_id) {
        this.user_request_id = user_request_id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }






    public static class Builder extends ChatMessage {
        private final int mType;
        private final String user_request_id;
        private final String type;
        private final String message;
        private final String room;


        public Builder(int mType, String user_request_id, String type, String message, String room) {
            this.mType = mType;
            this.user_request_id = user_request_id;
            this.type = type;
            this.message = message;
            this.room = room;
        }




        public ChatMessage build() {
            ChatMessage chatMessage = new ChatMessage();
            chatMessage.mType = mType;
            chatMessage.user_request_id = user_request_id;
            chatMessage.type = type;
            chatMessage.message = message;
            chatMessage.room = room;
            return chatMessage;
        }


       /*  public Builder(int type) {
            mType = type;
        }

       public Builder username(String username) {
            mUsername = username;
            return this;
        }

        public Builder message(String message) {
            mMessage = message;
            return this;
        }

        public ChatMessage build() {
            ChatMessage message = new ChatMessage();
            message.mType = mType;
            message.mUsername = mUsername;
            message.mMessage = mMessage;
            return message;
        }*/
    }
}

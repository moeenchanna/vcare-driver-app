package com.easy.vcare.driver.base;

import android.app.Activity;

public interface BaseView {

    Activity baseActivity();

    void showLoading();

    void hideLoading() throws Exception;

    void onSuccessLogout(Object object);

    void onError(Throwable throwable);
}

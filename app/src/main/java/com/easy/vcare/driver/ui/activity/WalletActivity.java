package com.easy.vcare.driver.ui.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.adapter.WalletViewAdapter;
import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.data.responses.WalletViewResponse;
import com.easy.vcare.driver.remote.ApiInterface;
import com.easy.vcare.driver.remote.ErrorUtils;
import com.easy.vcare.driver.remote.RetrofitClient;
import com.easy.vcare.driver.ui.activity.wallet.WalletViewActivity;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletActivity extends BaseActivity {

    private RecyclerView mRecyler;
    WalletViewAdapter walletViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        initView();

        initView();
        mainIntent = getIntent();
        extras = mainIntent.getExtras();
        assert extras != null;
        AUTHORIZE_TOKEN = extras.getString("token");

        Log.e(TAG, "AUTHORIZE_TOKEN: " + AUTHORIZE_TOKEN);
    }

    private void initView() {
        mRecyler = (RecyclerView) findViewById(R.id.recyler);
        mRecyler.setHasFixedSize(true);
        //set a vertical layout so the list is displayed top down
        final LinearLayoutManager layoutManager = new LinearLayoutManager(WalletActivity.this, LinearLayoutManager.VERTICAL, false);
        mRecyler.setLayoutManager(layoutManager);

       /* RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(WalletViewActivity.this);


        mRecyclerviewwWallet.setLayoutManager(mLayoutManager);
        mRecyclerviewwWallet.setAdapter(walletViewAdapter);
        mRecyclerviewwWallet.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(mRecyclerviewwWallet, false);
        mRecyclerviewwWallet.setHasFixedSize(true);
        mRecyclerviewwWallet.setItemViewCacheSize(20);
        mRecyclerviewwWallet.setDrawingCacheEnabled(true);
        mRecyclerviewwWallet.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);*/


        showProgressDialog("Loading Services", "Please wait...");
        getWalletData();
    }

    private void getWalletData() {

        Log.e(TAG, "getWalletData: ");

        ApiInterface apiInterface = RetrofitClient.createService(ApiInterface.class);
        Call<WalletViewResponse> call = apiInterface.getwalletView();

        Log.e(TAG, "registerRequest: " + call.toString());

        call.enqueue(new Callback<WalletViewResponse>() {
            @Override
            public void onResponse(@NotNull Call<WalletViewResponse> call, @NotNull Response<WalletViewResponse> response) {                Log.e(TAG, "onResponse: "+response );

                try {
                    if (response.isSuccessful()) {
                        hideProgressDialog();
                        Log.e(TAG, "onResponse: " + response);

                        assert response.body() != null;

                        Log.e(TAG, "onResponse: " + response.body().getMessage());
                        Log.e(TAG, "onResponse: " + response.body().getStatus());

                        if (response.body().getStatus() == 200) {
                            hideProgressDialog();
                            Log.e(TAG, "onResponse: " + response);

                            WalletViewResponse walletViewResponse = response.body();
                            if (walletViewResponse != null) {

                                String response_msg = walletViewResponse.getMessage();
                                Log.e(TAG, "onResponse: " + response_msg);

                                List<WalletViewResponse.Datum> walletViewResponses;
                                walletViewResponses = response.body().getData();

                                walletViewAdapter = new WalletViewAdapter(walletViewResponses, WalletActivity.this);
                                mRecyler.setAdapter(walletViewAdapter);
                                walletViewAdapter.setWalletList(walletViewResponses);


                                toastCall(response_msg);
                                snackbarCall(response_msg);


                            }

                        } else {
                            if (response.errorBody() != null) {

                                Gson gson = new Gson();
                                ErrorUtils message = gson.fromJson(response.errorBody().charStream(), ErrorUtils.class);

                                Log.e(TAG, "Error Response: " + message.getStatus());
                                Log.e(TAG, "Error Response: " + message.getMessage());


                                Toast.makeText(WalletActivity.this, message.getMessage(), Toast.LENGTH_SHORT).show();


                            }
                        }
                    }
                } catch (Exception e) {
                    hideProgressDialog();
                    Log.e(TAG, "catch: ", e);

                }

            }
            @Override
            public void onFailure(Call<WalletViewResponse> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "onFailure: ", t);
                Log.d("error", Objects.requireNonNull(t.getMessage()));
                toastCall("Server Error");
            }
        });
    }
}
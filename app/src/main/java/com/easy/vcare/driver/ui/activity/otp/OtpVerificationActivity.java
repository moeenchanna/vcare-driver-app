package com.easy.vcare.driver.ui.activity.otp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.data.requests.SendOtpRequest;
import com.easy.vcare.driver.data.requests.SignupRequest;
import com.easy.vcare.driver.data.responses.SendOtpResponse;
import com.easy.vcare.driver.data.responses.SignupResponse;
import com.easy.vcare.driver.remote.ApiInterface;
import com.easy.vcare.driver.remote.ErrorUtils;
import com.easy.vcare.driver.remote.ServiceGenerator;
import com.easy.vcare.driver.ui.activity.MainActivity;
import com.easy.vcare.driver.ui.activity.profile.ProfileViewActivity;
import com.easy.vcare.driver.ui.activity.ride.RideHistoryActivity;
import com.easy.vcare.driver.ui.activity.signup.SignupFormActivity;
import com.easy.vcare.driver.ui.activity.wallet.WalletViewActivity;
import com.goodiebag.pinview.Pinview;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpVerificationActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "OtpVerificationActivity";

    String name, email, password, contact, address, license, cnic;

    private TextView mButtonResend;
    private Button mButtonVerify;
    private Pinview mPinview;
    private TextView mNumberTv;
    String responseOtp, userOtp;


    Activity context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        statusbarTheme();
        setContentView(R.layout.activity_otp_verification);
        initView();

    }

    private void initView() {
        mButtonResend = (TextView) findViewById(R.id.resend_button);
        mButtonResend.setOnClickListener(this);
        mButtonVerify = (Button) findViewById(R.id.verify_button);
        mButtonVerify.setOnClickListener(this);
        mPinview = (Pinview) findViewById(R.id.pinview);
        mNumberTv = (TextView) findViewById(R.id.tv_number);


        mainIntent = getIntent();
        extras = mainIntent.getExtras();
        assert extras != null;
        name = extras.getString("name");
        email = extras.getString("email");
        password = extras.getString("password");
        contact = extras.getString("number");


        mNumberTv.setText("Verification code has been sent to " + contact);


        sendOtpCode(contact);
        Log.e(TAG, "initView: " + contact);

        mPinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {

                userOtp = mPinview.getValue();
                Log.e(TAG, "pin no: " + userOtp);
                checkVerificationOTP(userOtp);

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.resend_button:
                // TODO 20/06/
                hideKeyboardwithoutPopulate(context);

                toastCall("Resend");
                mPinview.setValue("");
                sendOtpCode(contact);


                break;
            case R.id.verify_button:
                // TODO 20/06/26

                if (mPinview.getValue().isEmpty() || mPinview.getValue().length() < 4) {


                    toastCall("Please enter valid OTP Code");
                    snackbarCall("Please enter valid OTP Code");

                    return;
                } else {

                    userOtp = mPinview.getValue();
                    Log.e(TAG, "userOtp no: " + userOtp);
                    checkVerificationOTP(userOtp);
                }

                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intentTransition();
    }


    /*
        public void checkVerificationOTP(String otpCODE) {
            if (otpCODE.equals(responseOtp)) {
                Log.d(TAG, "otpCODE: " + otpCODE);
                Log.d(TAG, "getcode: " + responseOtp);
                Log.e(TAG, "Match: ");

                hideKeyboardwithoutPopulate(context);

                showProgressDialog("Authentication","Please wait...");
                registerRequest(name, email, password, contact, address, license,cnic);

            } else {
                Log.d(TAG, "otpCODE: " + otpCODE);
                Log.d(TAG, "getcode: " + responseOtp);
                //toastCall("Invalid code. Please enter valid code.");
                snackbarCall("Invalid code. Please enter valid code.");

            }

        }*/
    private void sendOtpCode(String contact) {

        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<SendOtpResponse> call = apiInterface.sendOTP(new SendOtpRequest(contact));
        call.enqueue(new Callback<SendOtpResponse>() {


            @Override
            public void onResponse(@NotNull Call<SendOtpResponse> call, @NotNull Response<SendOtpResponse> response) {

                Log.e(TAG, "onResponse: " + response);

                if (response.isSuccessful() && response.body() != null) {

                    Log.e(TAG, "onResponse: " + response);

                    assert response.body() != null;

                    Log.e(TAG, "onResponse: " + response.body().getMessage());
                    Log.e(TAG, "onResponse: " + response.body().getStatus());

                    if (response.body().getStatus() == 200) {

                        Log.e(TAG, "onResponse: " + response);

                        SendOtpResponse verifyOtpResponse = response.body();

                        if (verifyOtpResponse != null) {

                            // responseOtp = String.valueOf(verifyOtpResponse.getData().get(0));
                            responseOtp = "1234";


                            String response_msg = verifyOtpResponse.getMessage();
                            Log.e(TAG, "onResponse: " + response_msg);

                            toastCall(response_msg);
                            snackbarCall(response_msg);
                        }
                    } else {
                        if (response.errorBody() != null) {

                            Gson gson = new Gson();
                            ErrorUtils message = gson.fromJson(response.errorBody().charStream(), ErrorUtils.class);

                            Log.e(TAG, "Error Response: " + message.getStatus());
                            Log.e(TAG, "Error Response: " + message.getMessage());


                            Toast.makeText(OtpVerificationActivity.this, message.getMessage(), Toast.LENGTH_SHORT).show();


                        }
                    }

                }
            }

            @Override
            public void onFailure(@NotNull Call<SendOtpResponse> call, @NotNull Throwable t) {

            }
        });
    }

    public void checkVerificationOTP(String otpCODE) {
        if (otpCODE.equals(responseOtp)) {
            Log.d(TAG, "otpCODE: " + otpCODE);
            Log.d(TAG, "getcode: " + responseOtp);
            Log.e(TAG, "Match: ");

            hideKeyboardwithoutPopulate(context);
            /*showProgressDialog("Loading", "Please wait...");
            loginVerify(otpCODE, phone);*/
            // Set up progress before call
            //showProgressDialog("Authentication", "Please wait...");
           // registerRequest(name, email, password, contact);
            mainIntent = new Intent(OtpVerificationActivity.this, SignupFormActivity.class);
            mainIntent.putExtra("name", name);
            mainIntent.putExtra("email", email);
            mainIntent.putExtra("password", password);
            mainIntent.putExtra("contact", contact);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
            OtpVerificationActivity.this.startActivity(mainIntent);

        } else {
            Log.d(TAG, "otpCODE: " + otpCODE);
            Log.d(TAG, "getcode: " + responseOtp);
            //toastCall("Invalid code. Please enter valid code.");
            snackbarCall("Invalid code. Please enter valid code.");

        }

    }

/*
    public void registerRequest(String name, String email, String password, String contact) {
        Log.e(TAG, "registerRequest: ");

        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);

        Call<SignupResponse> call = apiInterface.getRegister(new SignupRequest(name, email, password, contact, platform, deviceid));

        Log.e(TAG, "registerRequest: " + call.toString());

        call.enqueue(new Callback<SignupResponse>() {

            @Override
            public void onResponse(@NotNull Call<SignupResponse> call, @NotNull Response<SignupResponse> response) {

                Log.e(TAG, "onResponse: " + response);

                if (response.isSuccessful() && response.body() != null) {

                    hideProgressDialog();

                    Log.e(TAG, "onResponse: " + response);

                    assert response.body() != null;

                    Log.e(TAG, "onResponse: " + response.body().getMessage());
                    Log.e(TAG, "onResponse: " + response.body().getStatus());

                    if (response.body().getStatus() == 200) {

                        Log.e(TAG, "onResponse: " + response);

                        SignupResponse signupResponse = response.body();

                        if (signupResponse != null) {

                            String response_msg = signupResponse.getMessage();

                          //  String token = signupResponse.getData().get(0).getToken();

                            //Save Token
                            Log.e(TAG, "name: " + name);
                           // Log.e(TAG, "auth token: " + token);


                            toastCall(response_msg);
                            snackbarCall(response_msg);


                          */
/*  mainIntent = new Intent(OtpVerificationActivity.this, MainActivity.class);
                            //mainIntent.putExtra("token", token);
                            mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            OtpVerificationActivity.this.startActivity(mainIntent);
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                            finish();*//*

                        }
                    }
                } else {
                    if (response.errorBody() != null) {

                        Gson gson = new Gson();
                        ErrorUtils message = gson.fromJson(response.errorBody().charStream(), ErrorUtils.class);

                        Log.e(TAG, "Error Response: " + message.getStatus());
                        Log.e(TAG, "Error Response: " + message.getMessage());


                        Toast.makeText(OtpVerificationActivity.this, message.getMessage(), Toast.LENGTH_SHORT).show();


                    }
                }
                      */
/*  Gson gson = new Gson();
                        Type type = new TypeToken<SignupResponse>() {}.getType();

                        assert response.errorBody() != null;
                        SignupResponse signupResponse = gson.fromJson(response.errorBody().charStream(),type);

                        if (signupResponse != null) {

                            Log.e(TAG, "onResponse: "+ signupResponse.getErrors().toString() );

                        }*//*



            }

            @Override
            public void onFailure(@NotNull Call<SignupResponse> call, @NotNull Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "onFailure: ", t);
                Log.d("error", Objects.requireNonNull(t.getMessage()));
                toastCall("Server Error");
            }
        });
    }
*/

}

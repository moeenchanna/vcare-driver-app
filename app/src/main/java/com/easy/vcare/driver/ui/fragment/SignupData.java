package com.easy.vcare.driver.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.easy.vcare.driver.R;


public class SignupData extends Fragment {
    private static final String TAG = "SignupData";

    private ImageView mButtonNext;
    private EditText mAddressTxt;
    private EditText mLicenseTxt;
    private EditText mCnicTxt;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_signup_data, container, false);
        mButtonNext = view.findViewById(R.id.next);

        initView(view);
        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: ");
                Fragment thirdFragment = new SignupDocument();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, thirdFragment);
                transaction.addToBackStack(null);
                transaction.commit();
              /*  String address = mAddressTxt.getText().toString();
                String license = mLicenseTxt.getText().toString();
                String cnic = mCnicTxt.getText().toString();*/

           /*     Intent intent = new Intent();
                intent.putExtra("address", address);
                intent.putExtra("license", license);
                intent.putExtra("cnic", cnic);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                SignupData.this.startActivity(intent);*/


            }
        });
        return view;
    }

    private void initView(@NonNull final View itemView) {
        mAddressTxt = (EditText) itemView.findViewById(R.id.txt_address);
        mLicenseTxt = (EditText) itemView.findViewById(R.id.txt_license);
        mCnicTxt = (EditText) itemView.findViewById(R.id.txt_cnic);
    }

}

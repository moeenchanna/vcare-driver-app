package com.easy.vcare.driver.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.R;
import com.easy.vcare.driver.base.BaseActivity;

public class Dashboard extends BaseActivity {

    private static final String TAG = "Dashboard";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mainIntent = new Intent(Dashboard.this, MainActivity.class);


    }
}
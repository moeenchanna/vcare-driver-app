package com.easy.vcare.driver.ui.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.easy.vcare.driver.R;
import com.easy.vcare.driver.base.BaseActivity;
import com.easy.vcare.driver.data.requests.LoginPasswordRequest;
import com.easy.vcare.driver.data.responses.LoginpasswordResponse;
import com.easy.vcare.driver.remote.ApiInterface;
import com.easy.vcare.driver.remote.ServiceGenerator;
import com.easy.vcare.driver.ui.activity.forgotpassword.ForgotPasswordActivity;
import com.easy.vcare.driver.ui.activity.otp.LoginOtpActivity;
import com.easy.vcare.driver.ui.activity.signup.RegisterActivity;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";

    private Button mLoginButton;
    private TextView mAccountCreate;
    private EditText contact, password;
    private EditText mNumberLogin;
    private TextView mPassForgot;
    String number;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {

        mLoginButton = (Button) findViewById(R.id.login_button);
        mLoginButton.setOnClickListener(this);
        mAccountCreate = (TextView) findViewById(R.id.create_account);
        mAccountCreate.setOnClickListener(this);
        password = (EditText) findViewById(R.id.login_pass);
        mNumberLogin = (EditText) findViewById(R.id.login_number);


        mPassForgot = (TextView) findViewById(R.id.forgot_pass);
        mPassForgot.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create_account:
                // TODO 20/06/26

                mainIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                LoginActivity.this.startActivity(mainIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.login_button:
                // TODO 20/06/26

                showProgressDialog("Authentication", "Please wait...");
                 number = mNumberLogin.getText().toString();
                String pass = password.getText().toString();
                if (pass.isEmpty()) {
                    mainIntent = new Intent(LoginActivity.this, LoginOtpActivity.class);
                    mainIntent.putExtra("number", number);
                    mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    LoginActivity.this.startActivity(mainIntent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                } else {
                    loginRequest(number, pass);

                }

            /*  String number = contact.getText().toString();
              String pass = password.getText().toString();
                if (pass.isEmpty()) {
                            mainIntent = new Intent(LoginActivity.this, LoginOtpActivity.class);
                    mainIntent.putExtra("number", number);
                    mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    LoginActivity.this.startActivity(mainIntent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }else

            {
                loginRequest(number,pass);

            }*/

                break;
            case R.id.forgot_pass:// TODO 20/12/30
                mainIntent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                mainIntent.putExtra("number", number);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                LoginActivity.this.startActivity(mainIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            default:
                break;
        }
    }

    private void loginRequest(String number, String pass) {

        ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class);
        Call<LoginpasswordResponse> call = apiInterface.getLogin(new LoginPasswordRequest(pass, number));
        call.enqueue(new Callback<LoginpasswordResponse>() {

            @Override
            public void onResponse(@NotNull Call<LoginpasswordResponse> call, @NotNull Response<LoginpasswordResponse> response) {


                if (response.isSuccessful()) {

                    hideProgressDialog();

                    Log.e(TAG, "onResponse: " + response);

                    assert response.body() != null;

                    Log.e(TAG, "onResponse: " + response.body().getMessage());
                    Log.e(TAG, "onResponse: " + response.body().getStatus());
                    String response_msg = response.body().getMessage();
                    toastCall(response_msg);
                    snackbarCall(response_msg);

                    if (response.body().getStatus() == 200) {
                        Log.e(TAG, "onResponse: " + "go otp");
                      /*  mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                        mainIntent.putExtra("number", number);
                        mainIntent.putExtra("password", pass);
                        mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        LoginActivity.this.startActivity(mainIntent);
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);*/

                    }

                } else {
                    toastCall("Error Response");
                    hideProgressDialog();
                }

            }

            @Override
            public void onFailure(@NotNull Call<LoginpasswordResponse> call, @NotNull Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "onFailure: ", t);
                Log.d("error", Objects.requireNonNull(t.getMessage()));
                toastCall("Server Error");
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intentTransition();
    }
}
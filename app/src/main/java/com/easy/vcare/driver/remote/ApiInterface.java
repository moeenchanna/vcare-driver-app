package com.easy.vcare.driver.remote;


import com.easy.vcare.driver.data.requests.ForgotRequest;
import com.easy.vcare.driver.data.requests.LoginPasswordRequest;
import com.easy.vcare.driver.data.requests.SendOtpRequest;
import com.easy.vcare.driver.data.requests.SignupRequest;
import com.easy.vcare.driver.data.requests.VerifyOtpRequest;
import com.easy.vcare.driver.data.responses.ForgotResponse;
import com.easy.vcare.driver.data.responses.LoginpasswordResponse;
import com.easy.vcare.driver.data.responses.ProfileViewResponse;
import com.easy.vcare.driver.data.responses.RideViewResponse;
import com.easy.vcare.driver.data.responses.SendOtpResponse;
import com.easy.vcare.driver.data.responses.SignupResponse;
import com.easy.vcare.driver.data.responses.VerifyOtpResponse;
import com.easy.vcare.driver.data.responses.WalletViewResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {
    @POST("register")
    Call<SignupResponse> getRegister(@Body SignupRequest signupRequest);

    @POST("login/password")
    Call<LoginpasswordResponse> getLogin(@Body LoginPasswordRequest loginRequest);

    @POST("send_otp")
    Call<SendOtpResponse> sendOTP(@Body SendOtpRequest sendOtpRequest);

    @POST("login/otp_verify")
    Call<VerifyOtpResponse> getLoginVerify(@Body VerifyOtpRequest verifyOtpRequest);

    @POST("forgot_password")
    Call<ForgotResponse> getForgotPassword(@Body ForgotRequest forgotRequest);

    @GET("profile")
    Call<ProfileViewResponse>getprofileView();

    @GET("wallets")
    Call<WalletViewResponse> getwalletView();
    @GET("rides")
    Call<RideViewResponse> getRideHistory();



    }